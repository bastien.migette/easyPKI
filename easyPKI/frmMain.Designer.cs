﻿namespace easyPKI {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabCA = new System.Windows.Forms.TabPage();
            this.grpSubCA = new System.Windows.Forms.GroupBox();
            this.btnExportCACert = new System.Windows.Forms.Button();
            this.btnExportSubCAPVK = new System.Windows.Forms.Button();
            this.tvSubCAs = new System.Windows.Forms.TreeView();
            this.rtbSubCA = new System.Windows.Forms.RichTextBox();
            this.btnDeleteSubCA = new System.Windows.Forms.Button();
            this.btnGenerateSubCACert = new System.Windows.Forms.Button();
            this.cmbSubCAASigAlgo = new System.Windows.Forms.ComboBox();
            this.numSubCAKeySize = new System.Windows.Forms.NumericUpDown();
            this.numSubCAValidity = new System.Windows.Forms.NumericUpDown();
            this.txtSubCASubject = new System.Windows.Forms.TextBox();
            this.lblSubCAKeySize = new System.Windows.Forms.Label();
            this.lblSubCAValidity = new System.Windows.Forms.Label();
            this.lblSubCASigAlgo = new System.Windows.Forms.Label();
            this.lblSubCASubject = new System.Windows.Forms.Label();
            this.grpRootCA = new System.Windows.Forms.GroupBox();
            this.btnExportRootCACert = new System.Windows.Forms.Button();
            this.btnExportRootCAPVK = new System.Windows.Forms.Button();
            this.rtbRootCA = new System.Windows.Forms.RichTextBox();
            this.btnEraseRootCA = new System.Windows.Forms.Button();
            this.btnGenerateRootCACert = new System.Windows.Forms.Button();
            this.cmbCASigAlgo = new System.Windows.Forms.ComboBox();
            this.numRootCAKeySize = new System.Windows.Forms.NumericUpDown();
            this.numRootCAValidity = new System.Windows.Forms.NumericUpDown();
            this.txtCASubject = new System.Windows.Forms.TextBox();
            this.lblRootCAKeySize = new System.Windows.Forms.Label();
            this.lblRootCAValidity = new System.Windows.Forms.Label();
            this.lblSigAlgo = new System.Windows.Forms.Label();
            this.lblRootCASubject = new System.Windows.Forms.Label();
            this.tabClients = new System.Windows.Forms.TabPage();
            this.btnDeleteCert = new System.Windows.Forms.Button();
            this.btnCreateCert = new System.Windows.Forms.Button();
            this.btnLoadServerTemplate = new System.Windows.Forms.Button();
            this.btnLoadClientTemplate = new System.Windows.Forms.Button();
            this.btnExportCertChainPfx = new System.Windows.Forms.Button();
            this.btnExportCertPfx = new System.Windows.Forms.Button();
            this.btnExportCertChain = new System.Windows.Forms.Button();
            this.btnExportCert = new System.Windows.Forms.Button();
            this.btnExportCertPVK = new System.Windows.Forms.Button();
            this.btnDeleteAttribute = new System.Windows.Forms.Button();
            this.cmbAttributeValue = new System.Windows.Forms.ComboBox();
            this.btnAddAttribute = new System.Windows.Forms.Button();
            this.cmbAttribute = new System.Windows.Forms.ComboBox();
            this.rtbClientPrivateKey = new System.Windows.Forms.RichTextBox();
            this.lstClientAttributes = new System.Windows.Forms.ListBox();
            this.lstClientCerts = new System.Windows.Forms.ListBox();
            this.rtbClientCert = new System.Windows.Forms.RichTextBox();
            this.tvClientCA = new System.Windows.Forms.TreeView();
            this.cmbUserCertSigAlgo = new System.Windows.Forms.ComboBox();
            this.numUserCertKeySize = new System.Windows.Forms.NumericUpDown();
            this.numUserCertValidity = new System.Windows.Forms.NumericUpDown();
            this.txtUserCertSubject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.prgStartup = new System.Windows.Forms.ProgressBar();
            this.lblStartup = new System.Windows.Forms.Label();
            this.btnSignCSR = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tabControlMain.SuspendLayout();
            this.tabCA.SuspendLayout();
            this.grpSubCA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSubCAKeySize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubCAValidity)).BeginInit();
            this.grpRootCA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRootCAKeySize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRootCAValidity)).BeginInit();
            this.tabClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUserCertKeySize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserCertValidity)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabCA);
            this.tabControlMain.Controls.Add(this.tabClients);
            this.tabControlMain.Location = new System.Drawing.Point(13, 13);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1061, 475);
            this.tabControlMain.TabIndex = 1;
            // 
            // tabCA
            // 
            this.tabCA.Controls.Add(this.grpSubCA);
            this.tabCA.Controls.Add(this.grpRootCA);
            this.tabCA.Location = new System.Drawing.Point(4, 22);
            this.tabCA.Name = "tabCA";
            this.tabCA.Padding = new System.Windows.Forms.Padding(3);
            this.tabCA.Size = new System.Drawing.Size(1053, 449);
            this.tabCA.TabIndex = 0;
            this.tabCA.Text = "CA / Sub CAs";
            this.tabCA.UseVisualStyleBackColor = true;
            // 
            // grpSubCA
            // 
            this.grpSubCA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSubCA.Controls.Add(this.btnExportCACert);
            this.grpSubCA.Controls.Add(this.btnExportSubCAPVK);
            this.grpSubCA.Controls.Add(this.tvSubCAs);
            this.grpSubCA.Controls.Add(this.rtbSubCA);
            this.grpSubCA.Controls.Add(this.btnDeleteSubCA);
            this.grpSubCA.Controls.Add(this.btnGenerateSubCACert);
            this.grpSubCA.Controls.Add(this.cmbSubCAASigAlgo);
            this.grpSubCA.Controls.Add(this.numSubCAKeySize);
            this.grpSubCA.Controls.Add(this.numSubCAValidity);
            this.grpSubCA.Controls.Add(this.txtSubCASubject);
            this.grpSubCA.Controls.Add(this.lblSubCAKeySize);
            this.grpSubCA.Controls.Add(this.lblSubCAValidity);
            this.grpSubCA.Controls.Add(this.lblSubCASigAlgo);
            this.grpSubCA.Controls.Add(this.lblSubCASubject);
            this.grpSubCA.Location = new System.Drawing.Point(370, 6);
            this.grpSubCA.Name = "grpSubCA";
            this.grpSubCA.Size = new System.Drawing.Size(692, 433);
            this.grpSubCA.TabIndex = 21;
            this.grpSubCA.TabStop = false;
            this.grpSubCA.Text = "Sub CAs";
            // 
            // btnExportCACert
            // 
            this.btnExportCACert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportCACert.Location = new System.Drawing.Point(553, 396);
            this.btnExportCACert.Name = "btnExportCACert";
            this.btnExportCACert.Size = new System.Drawing.Size(122, 23);
            this.btnExportCACert.TabIndex = 24;
            this.btnExportCACert.Text = "Export Sub CA Cert";
            this.btnExportCACert.UseVisualStyleBackColor = true;
            this.btnExportCACert.Click += new System.EventHandler(this.btnExportCACert_Click);
            // 
            // btnExportSubCAPVK
            // 
            this.btnExportSubCAPVK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportSubCAPVK.Location = new System.Drawing.Point(425, 396);
            this.btnExportSubCAPVK.Name = "btnExportSubCAPVK";
            this.btnExportSubCAPVK.Size = new System.Drawing.Size(122, 23);
            this.btnExportSubCAPVK.TabIndex = 23;
            this.btnExportSubCAPVK.Text = "Export Sub CA PVK";
            this.btnExportSubCAPVK.UseVisualStyleBackColor = true;
            this.btnExportSubCAPVK.Click += new System.EventHandler(this.btnExportSubCAPVK_Click);
            // 
            // tvSubCAs
            // 
            this.tvSubCAs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvSubCAs.FullRowSelect = true;
            this.tvSubCAs.HideSelection = false;
            this.tvSubCAs.Location = new System.Drawing.Point(6, 18);
            this.tvSubCAs.Name = "tvSubCAs";
            this.tvSubCAs.Size = new System.Drawing.Size(334, 401);
            this.tvSubCAs.TabIndex = 21;
            this.tvSubCAs.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvSubCAs_AfterSelect);
            // 
            // rtbSubCA
            // 
            this.rtbSubCA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbSubCA.Location = new System.Drawing.Point(347, 186);
            this.rtbSubCA.Name = "rtbSubCA";
            this.rtbSubCA.Size = new System.Drawing.Size(328, 204);
            this.rtbSubCA.TabIndex = 20;
            this.rtbSubCA.Text = "";
            // 
            // btnDeleteSubCA
            // 
            this.btnDeleteSubCA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSubCA.Location = new System.Drawing.Point(510, 144);
            this.btnDeleteSubCA.Name = "btnDeleteSubCA";
            this.btnDeleteSubCA.Size = new System.Drawing.Size(165, 23);
            this.btnDeleteSubCA.TabIndex = 19;
            this.btnDeleteSubCA.Text = "Erase Sub CA";
            this.btnDeleteSubCA.UseVisualStyleBackColor = true;
            this.btnDeleteSubCA.Click += new System.EventHandler(this.btnDeleteSubCA_Click);
            // 
            // btnGenerateSubCACert
            // 
            this.btnGenerateSubCACert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateSubCACert.Location = new System.Drawing.Point(347, 144);
            this.btnGenerateSubCACert.Name = "btnGenerateSubCACert";
            this.btnGenerateSubCACert.Size = new System.Drawing.Size(162, 23);
            this.btnGenerateSubCACert.TabIndex = 18;
            this.btnGenerateSubCACert.Text = "Generate SubCA Cert";
            this.btnGenerateSubCACert.UseVisualStyleBackColor = true;
            this.btnGenerateSubCACert.Click += new System.EventHandler(this.btnGenerateSubCACert_Click);
            // 
            // cmbSubCAASigAlgo
            // 
            this.cmbSubCAASigAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSubCAASigAlgo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubCAASigAlgo.FormattingEnabled = true;
            this.cmbSubCAASigAlgo.Location = new System.Drawing.Point(436, 47);
            this.cmbSubCAASigAlgo.Name = "cmbSubCAASigAlgo";
            this.cmbSubCAASigAlgo.Size = new System.Drawing.Size(239, 21);
            this.cmbSubCAASigAlgo.TabIndex = 17;
            // 
            // numSubCAKeySize
            // 
            this.numSubCAKeySize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numSubCAKeySize.Increment = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numSubCAKeySize.Location = new System.Drawing.Point(436, 100);
            this.numSubCAKeySize.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.numSubCAKeySize.Minimum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numSubCAKeySize.Name = "numSubCAKeySize";
            this.numSubCAKeySize.Size = new System.Drawing.Size(239, 20);
            this.numSubCAKeySize.TabIndex = 16;
            this.numSubCAKeySize.Value = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            // 
            // numSubCAValidity
            // 
            this.numSubCAValidity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numSubCAValidity.Location = new System.Drawing.Point(436, 74);
            this.numSubCAValidity.Name = "numSubCAValidity";
            this.numSubCAValidity.Size = new System.Drawing.Size(239, 20);
            this.numSubCAValidity.TabIndex = 15;
            this.numSubCAValidity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // txtSubCASubject
            // 
            this.txtSubCASubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubCASubject.Location = new System.Drawing.Point(436, 21);
            this.txtSubCASubject.Name = "txtSubCASubject";
            this.txtSubCASubject.Size = new System.Drawing.Size(239, 20);
            this.txtSubCASubject.TabIndex = 14;
            this.txtSubCASubject.Text = "CN=Sub CA";
            // 
            // lblSubCAKeySize
            // 
            this.lblSubCAKeySize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubCAKeySize.AutoSize = true;
            this.lblSubCAKeySize.Location = new System.Drawing.Point(348, 102);
            this.lblSubCAKeySize.Name = "lblSubCAKeySize";
            this.lblSubCAKeySize.Size = new System.Drawing.Size(43, 13);
            this.lblSubCAKeySize.TabIndex = 13;
            this.lblSubCAKeySize.Text = "Keysize";
            // 
            // lblSubCAValidity
            // 
            this.lblSubCAValidity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubCAValidity.AutoSize = true;
            this.lblSubCAValidity.Location = new System.Drawing.Point(348, 76);
            this.lblSubCAValidity.Name = "lblSubCAValidity";
            this.lblSubCAValidity.Size = new System.Drawing.Size(74, 13);
            this.lblSubCAValidity.TabIndex = 12;
            this.lblSubCAValidity.Text = "Validity (years)";
            // 
            // lblSubCASigAlgo
            // 
            this.lblSubCASigAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubCASigAlgo.AutoSize = true;
            this.lblSubCASigAlgo.Location = new System.Drawing.Point(348, 50);
            this.lblSubCASigAlgo.Name = "lblSubCASigAlgo";
            this.lblSubCASigAlgo.Size = new System.Drawing.Size(79, 13);
            this.lblSubCASigAlgo.TabIndex = 11;
            this.lblSubCASigAlgo.Text = "Signature Algo.";
            // 
            // lblSubCASubject
            // 
            this.lblSubCASubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubCASubject.AutoSize = true;
            this.lblSubCASubject.Location = new System.Drawing.Point(348, 24);
            this.lblSubCASubject.Name = "lblSubCASubject";
            this.lblSubCASubject.Size = new System.Drawing.Size(82, 13);
            this.lblSubCASubject.TabIndex = 10;
            this.lblSubCASubject.Text = "CA Cert Subject";
            // 
            // grpRootCA
            // 
            this.grpRootCA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.grpRootCA.Controls.Add(this.btnExportRootCACert);
            this.grpRootCA.Controls.Add(this.btnExportRootCAPVK);
            this.grpRootCA.Controls.Add(this.rtbRootCA);
            this.grpRootCA.Controls.Add(this.btnEraseRootCA);
            this.grpRootCA.Controls.Add(this.btnGenerateRootCACert);
            this.grpRootCA.Controls.Add(this.cmbCASigAlgo);
            this.grpRootCA.Controls.Add(this.numRootCAKeySize);
            this.grpRootCA.Controls.Add(this.numRootCAValidity);
            this.grpRootCA.Controls.Add(this.txtCASubject);
            this.grpRootCA.Controls.Add(this.lblRootCAKeySize);
            this.grpRootCA.Controls.Add(this.lblRootCAValidity);
            this.grpRootCA.Controls.Add(this.lblSigAlgo);
            this.grpRootCA.Controls.Add(this.lblRootCASubject);
            this.grpRootCA.Location = new System.Drawing.Point(6, 6);
            this.grpRootCA.Name = "grpRootCA";
            this.grpRootCA.Size = new System.Drawing.Size(358, 433);
            this.grpRootCA.TabIndex = 10;
            this.grpRootCA.TabStop = false;
            this.grpRootCA.Text = "Root CA";
            // 
            // btnExportRootCACert
            // 
            this.btnExportRootCACert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportRootCACert.Location = new System.Drawing.Point(247, 396);
            this.btnExportRootCACert.Name = "btnExportRootCACert";
            this.btnExportRootCACert.Size = new System.Drawing.Size(99, 23);
            this.btnExportRootCACert.TabIndex = 22;
            this.btnExportRootCACert.Text = "Export CA Cert";
            this.btnExportRootCACert.UseVisualStyleBackColor = true;
            this.btnExportRootCACert.Click += new System.EventHandler(this.btnExportRootCACert_Click);
            // 
            // btnExportRootCAPVK
            // 
            this.btnExportRootCAPVK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportRootCAPVK.Location = new System.Drawing.Point(142, 396);
            this.btnExportRootCAPVK.Name = "btnExportRootCAPVK";
            this.btnExportRootCAPVK.Size = new System.Drawing.Size(99, 23);
            this.btnExportRootCAPVK.TabIndex = 21;
            this.btnExportRootCAPVK.Text = "Export CA PVK";
            this.btnExportRootCAPVK.UseVisualStyleBackColor = true;
            this.btnExportRootCAPVK.Click += new System.EventHandler(this.btnExportRootCAPVK_Click);
            // 
            // rtbRootCA
            // 
            this.rtbRootCA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbRootCA.Location = new System.Drawing.Point(7, 186);
            this.rtbRootCA.Name = "rtbRootCA";
            this.rtbRootCA.Size = new System.Drawing.Size(339, 204);
            this.rtbRootCA.TabIndex = 20;
            this.rtbRootCA.Text = "";
            // 
            // btnEraseRootCA
            // 
            this.btnEraseRootCA.Location = new System.Drawing.Point(178, 144);
            this.btnEraseRootCA.Name = "btnEraseRootCA";
            this.btnEraseRootCA.Size = new System.Drawing.Size(168, 23);
            this.btnEraseRootCA.TabIndex = 19;
            this.btnEraseRootCA.Text = "Erase CA";
            this.btnEraseRootCA.UseVisualStyleBackColor = true;
            this.btnEraseRootCA.Click += new System.EventHandler(this.btnEraseRootCA_Click);
            // 
            // btnGenerateRootCACert
            // 
            this.btnGenerateRootCACert.Location = new System.Drawing.Point(6, 144);
            this.btnGenerateRootCACert.Name = "btnGenerateRootCACert";
            this.btnGenerateRootCACert.Size = new System.Drawing.Size(165, 23);
            this.btnGenerateRootCACert.TabIndex = 18;
            this.btnGenerateRootCACert.Text = "Generate CA Cert";
            this.btnGenerateRootCACert.UseVisualStyleBackColor = true;
            this.btnGenerateRootCACert.Click += new System.EventHandler(this.btnGenerateRootCACert_Click);
            // 
            // cmbCASigAlgo
            // 
            this.cmbCASigAlgo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCASigAlgo.FormattingEnabled = true;
            this.cmbCASigAlgo.Location = new System.Drawing.Point(96, 47);
            this.cmbCASigAlgo.Name = "cmbCASigAlgo";
            this.cmbCASigAlgo.Size = new System.Drawing.Size(250, 21);
            this.cmbCASigAlgo.TabIndex = 17;
            // 
            // numRootCAKeySize
            // 
            this.numRootCAKeySize.Increment = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numRootCAKeySize.Location = new System.Drawing.Point(96, 100);
            this.numRootCAKeySize.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.numRootCAKeySize.Minimum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numRootCAKeySize.Name = "numRootCAKeySize";
            this.numRootCAKeySize.Size = new System.Drawing.Size(250, 20);
            this.numRootCAKeySize.TabIndex = 16;
            this.numRootCAKeySize.Value = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            // 
            // numRootCAValidity
            // 
            this.numRootCAValidity.Location = new System.Drawing.Point(96, 74);
            this.numRootCAValidity.Name = "numRootCAValidity";
            this.numRootCAValidity.Size = new System.Drawing.Size(250, 20);
            this.numRootCAValidity.TabIndex = 15;
            this.numRootCAValidity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // txtCASubject
            // 
            this.txtCASubject.Location = new System.Drawing.Point(96, 21);
            this.txtCASubject.Name = "txtCASubject";
            this.txtCASubject.Size = new System.Drawing.Size(250, 20);
            this.txtCASubject.TabIndex = 14;
            this.txtCASubject.Text = "CN=Root CA";
            // 
            // lblRootCAKeySize
            // 
            this.lblRootCAKeySize.AutoSize = true;
            this.lblRootCAKeySize.Location = new System.Drawing.Point(8, 102);
            this.lblRootCAKeySize.Name = "lblRootCAKeySize";
            this.lblRootCAKeySize.Size = new System.Drawing.Size(43, 13);
            this.lblRootCAKeySize.TabIndex = 13;
            this.lblRootCAKeySize.Text = "Keysize";
            // 
            // lblRootCAValidity
            // 
            this.lblRootCAValidity.AutoSize = true;
            this.lblRootCAValidity.Location = new System.Drawing.Point(8, 76);
            this.lblRootCAValidity.Name = "lblRootCAValidity";
            this.lblRootCAValidity.Size = new System.Drawing.Size(74, 13);
            this.lblRootCAValidity.TabIndex = 12;
            this.lblRootCAValidity.Text = "Validity (years)";
            // 
            // lblSigAlgo
            // 
            this.lblSigAlgo.AutoSize = true;
            this.lblSigAlgo.Location = new System.Drawing.Point(8, 50);
            this.lblSigAlgo.Name = "lblSigAlgo";
            this.lblSigAlgo.Size = new System.Drawing.Size(79, 13);
            this.lblSigAlgo.TabIndex = 11;
            this.lblSigAlgo.Text = "Signature Algo.";
            // 
            // lblRootCASubject
            // 
            this.lblRootCASubject.AutoSize = true;
            this.lblRootCASubject.Location = new System.Drawing.Point(8, 24);
            this.lblRootCASubject.Name = "lblRootCASubject";
            this.lblRootCASubject.Size = new System.Drawing.Size(82, 13);
            this.lblRootCASubject.TabIndex = 10;
            this.lblRootCASubject.Text = "CA Cert Subject";
            // 
            // tabClients
            // 
            this.tabClients.Controls.Add(this.btnSignCSR);
            this.tabClients.Controls.Add(this.btnDeleteCert);
            this.tabClients.Controls.Add(this.btnCreateCert);
            this.tabClients.Controls.Add(this.btnLoadServerTemplate);
            this.tabClients.Controls.Add(this.btnLoadClientTemplate);
            this.tabClients.Controls.Add(this.btnExportCertChainPfx);
            this.tabClients.Controls.Add(this.btnExportCertPfx);
            this.tabClients.Controls.Add(this.btnExportCertChain);
            this.tabClients.Controls.Add(this.btnExportCert);
            this.tabClients.Controls.Add(this.btnExportCertPVK);
            this.tabClients.Controls.Add(this.btnDeleteAttribute);
            this.tabClients.Controls.Add(this.cmbAttributeValue);
            this.tabClients.Controls.Add(this.btnAddAttribute);
            this.tabClients.Controls.Add(this.cmbAttribute);
            this.tabClients.Controls.Add(this.rtbClientPrivateKey);
            this.tabClients.Controls.Add(this.lstClientAttributes);
            this.tabClients.Controls.Add(this.lstClientCerts);
            this.tabClients.Controls.Add(this.rtbClientCert);
            this.tabClients.Controls.Add(this.tvClientCA);
            this.tabClients.Controls.Add(this.cmbUserCertSigAlgo);
            this.tabClients.Controls.Add(this.numUserCertKeySize);
            this.tabClients.Controls.Add(this.numUserCertValidity);
            this.tabClients.Controls.Add(this.txtUserCertSubject);
            this.tabClients.Controls.Add(this.label1);
            this.tabClients.Controls.Add(this.label2);
            this.tabClients.Controls.Add(this.label3);
            this.tabClients.Controls.Add(this.label4);
            this.tabClients.Location = new System.Drawing.Point(4, 22);
            this.tabClients.Name = "tabClients";
            this.tabClients.Padding = new System.Windows.Forms.Padding(3);
            this.tabClients.Size = new System.Drawing.Size(1053, 449);
            this.tabClients.TabIndex = 1;
            this.tabClients.Text = "Client / Server certs";
            this.tabClients.UseVisualStyleBackColor = true;
            // 
            // btnDeleteCert
            // 
            this.btnDeleteCert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteCert.Location = new System.Drawing.Point(508, 414);
            this.btnDeleteCert.Name = "btnDeleteCert";
            this.btnDeleteCert.Size = new System.Drawing.Size(81, 23);
            this.btnDeleteCert.TabIndex = 48;
            this.btnDeleteCert.Text = "Delete Cert";
            this.btnDeleteCert.UseVisualStyleBackColor = true;
            this.btnDeleteCert.Click += new System.EventHandler(this.btnDeleteCert_Click);
            // 
            // btnCreateCert
            // 
            this.btnCreateCert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateCert.Location = new System.Drawing.Point(563, 118);
            this.btnCreateCert.Name = "btnCreateCert";
            this.btnCreateCert.Size = new System.Drawing.Size(72, 23);
            this.btnCreateCert.TabIndex = 47;
            this.btnCreateCert.Text = "Create Cert";
            this.btnCreateCert.UseVisualStyleBackColor = true;
            this.btnCreateCert.Click += new System.EventHandler(this.btnCreateCert_Click);
            // 
            // btnLoadServerTemplate
            // 
            this.btnLoadServerTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadServerTemplate.Location = new System.Drawing.Point(449, 118);
            this.btnLoadServerTemplate.Name = "btnLoadServerTemplate";
            this.btnLoadServerTemplate.Size = new System.Drawing.Size(108, 23);
            this.btnLoadServerTemplate.TabIndex = 46;
            this.btnLoadServerTemplate.Text = "Load Server Templ.";
            this.btnLoadServerTemplate.UseVisualStyleBackColor = true;
            this.btnLoadServerTemplate.Click += new System.EventHandler(this.btnLoadServerTemplate_Click);
            // 
            // btnLoadClientTemplate
            // 
            this.btnLoadClientTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadClientTemplate.Location = new System.Drawing.Point(337, 118);
            this.btnLoadClientTemplate.Name = "btnLoadClientTemplate";
            this.btnLoadClientTemplate.Size = new System.Drawing.Size(106, 23);
            this.btnLoadClientTemplate.TabIndex = 45;
            this.btnLoadClientTemplate.Text = "Load Client Templ.";
            this.btnLoadClientTemplate.UseVisualStyleBackColor = true;
            this.btnLoadClientTemplate.Click += new System.EventHandler(this.btnLoadClientTemplate_Click);
            // 
            // btnExportCertChainPfx
            // 
            this.btnExportCertChainPfx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportCertChainPfx.Location = new System.Drawing.Point(374, 414);
            this.btnExportCertChainPfx.Name = "btnExportCertChainPfx";
            this.btnExportCertChainPfx.Size = new System.Drawing.Size(131, 23);
            this.btnExportCertChainPfx.TabIndex = 44;
            this.btnExportCertChainPfx.Text = "Export Cert Chain (pfx)";
            this.btnExportCertChainPfx.UseVisualStyleBackColor = true;
            this.btnExportCertChainPfx.Click += new System.EventHandler(this.btnExportCertChainPfx_Click);
            // 
            // btnExportCertPfx
            // 
            this.btnExportCertPfx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportCertPfx.Location = new System.Drawing.Point(278, 414);
            this.btnExportCertPfx.Name = "btnExportCertPfx";
            this.btnExportCertPfx.Size = new System.Drawing.Size(93, 23);
            this.btnExportCertPfx.TabIndex = 43;
            this.btnExportCertPfx.Text = "Export Cert (pfx)";
            this.btnExportCertPfx.UseVisualStyleBackColor = true;
            this.btnExportCertPfx.Click += new System.EventHandler(this.btnExportCertPfx_Click);
            // 
            // btnExportCertChain
            // 
            this.btnExportCertChain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportCertChain.Location = new System.Drawing.Point(164, 414);
            this.btnExportCertChain.Name = "btnExportCertChain";
            this.btnExportCertChain.Size = new System.Drawing.Size(111, 23);
            this.btnExportCertChain.TabIndex = 42;
            this.btnExportCertChain.Text = "Export Cert Chain";
            this.btnExportCertChain.UseVisualStyleBackColor = true;
            this.btnExportCertChain.Click += new System.EventHandler(this.btnExportCertChain_Click);
            // 
            // btnExportCert
            // 
            this.btnExportCert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportCert.Location = new System.Drawing.Point(86, 414);
            this.btnExportCert.Name = "btnExportCert";
            this.btnExportCert.Size = new System.Drawing.Size(75, 23);
            this.btnExportCert.TabIndex = 41;
            this.btnExportCert.Text = "Export Cert";
            this.btnExportCert.UseVisualStyleBackColor = true;
            this.btnExportCert.Click += new System.EventHandler(this.btnExportCert_Click);
            // 
            // btnExportCertPVK
            // 
            this.btnExportCertPVK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportCertPVK.Location = new System.Drawing.Point(7, 414);
            this.btnExportCertPVK.Name = "btnExportCertPVK";
            this.btnExportCertPVK.Size = new System.Drawing.Size(75, 23);
            this.btnExportCertPVK.TabIndex = 40;
            this.btnExportCertPVK.Text = "Export PVK";
            this.btnExportCertPVK.UseVisualStyleBackColor = true;
            this.btnExportCertPVK.Click += new System.EventHandler(this.btnExportCertPVK_Click);
            // 
            // btnDeleteAttribute
            // 
            this.btnDeleteAttribute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAttribute.Location = new System.Drawing.Point(641, 171);
            this.btnDeleteAttribute.Name = "btnDeleteAttribute";
            this.btnDeleteAttribute.Size = new System.Drawing.Size(72, 23);
            this.btnDeleteAttribute.TabIndex = 39;
            this.btnDeleteAttribute.Text = "Delete";
            this.btnDeleteAttribute.UseVisualStyleBackColor = true;
            this.btnDeleteAttribute.Click += new System.EventHandler(this.btnDeleteAttribute_Click);
            // 
            // cmbAttributeValue
            // 
            this.cmbAttributeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAttributeValue.FormattingEnabled = true;
            this.cmbAttributeValue.Location = new System.Drawing.Point(337, 173);
            this.cmbAttributeValue.Name = "cmbAttributeValue";
            this.cmbAttributeValue.Size = new System.Drawing.Size(298, 21);
            this.cmbAttributeValue.TabIndex = 38;
            // 
            // btnAddAttribute
            // 
            this.btnAddAttribute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAttribute.Location = new System.Drawing.Point(641, 144);
            this.btnAddAttribute.Name = "btnAddAttribute";
            this.btnAddAttribute.Size = new System.Drawing.Size(72, 23);
            this.btnAddAttribute.TabIndex = 37;
            this.btnAddAttribute.Text = "Add";
            this.btnAddAttribute.UseVisualStyleBackColor = true;
            this.btnAddAttribute.Click += new System.EventHandler(this.btnAddAttribute_Click);
            // 
            // cmbAttribute
            // 
            this.cmbAttribute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAttribute.FormattingEnabled = true;
            this.cmbAttribute.Items.AddRange(new object[] {
            "Key Usage",
            "Extended Key Usage",
            "SAN - DNS",
            "SAN - IP"});
            this.cmbAttribute.Location = new System.Drawing.Point(337, 146);
            this.cmbAttribute.Name = "cmbAttribute";
            this.cmbAttribute.Size = new System.Drawing.Size(298, 21);
            this.cmbAttribute.TabIndex = 35;
            this.cmbAttribute.SelectedIndexChanged += new System.EventHandler(this.cmbAttribute_SelectedIndexChanged);
            // 
            // rtbClientPrivateKey
            // 
            this.rtbClientPrivateKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbClientPrivateKey.Location = new System.Drawing.Point(719, 238);
            this.rtbClientPrivateKey.Name = "rtbClientPrivateKey";
            this.rtbClientPrivateKey.Size = new System.Drawing.Size(326, 200);
            this.rtbClientPrivateKey.TabIndex = 34;
            this.rtbClientPrivateKey.Text = "";
            // 
            // lstClientAttributes
            // 
            this.lstClientAttributes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstClientAttributes.FormattingEnabled = true;
            this.lstClientAttributes.Location = new System.Drawing.Point(337, 200);
            this.lstClientAttributes.Name = "lstClientAttributes";
            this.lstClientAttributes.Size = new System.Drawing.Size(376, 199);
            this.lstClientAttributes.TabIndex = 33;
            // 
            // lstClientCerts
            // 
            this.lstClientCerts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstClientCerts.FormattingEnabled = true;
            this.lstClientCerts.Location = new System.Drawing.Point(7, 200);
            this.lstClientCerts.Name = "lstClientCerts";
            this.lstClientCerts.Size = new System.Drawing.Size(319, 199);
            this.lstClientCerts.TabIndex = 32;
            this.lstClientCerts.SelectedIndexChanged += new System.EventHandler(this.lstClientCerts_SelectedIndexChanged);
            // 
            // rtbClientCert
            // 
            this.rtbClientCert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbClientCert.Location = new System.Drawing.Point(719, 12);
            this.rtbClientCert.Name = "rtbClientCert";
            this.rtbClientCert.Size = new System.Drawing.Size(326, 220);
            this.rtbClientCert.TabIndex = 31;
            this.rtbClientCert.Text = "";
            // 
            // tvClientCA
            // 
            this.tvClientCA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvClientCA.FullRowSelect = true;
            this.tvClientCA.HideSelection = false;
            this.tvClientCA.Location = new System.Drawing.Point(7, 12);
            this.tvClientCA.Name = "tvClientCA";
            this.tvClientCA.Size = new System.Drawing.Size(319, 182);
            this.tvClientCA.TabIndex = 30;
            this.tvClientCA.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvClientCA_AfterSelect);
            // 
            // cmbUserCertSigAlgo
            // 
            this.cmbUserCertSigAlgo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUserCertSigAlgo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserCertSigAlgo.FormattingEnabled = true;
            this.cmbUserCertSigAlgo.Location = new System.Drawing.Point(422, 41);
            this.cmbUserCertSigAlgo.Name = "cmbUserCertSigAlgo";
            this.cmbUserCertSigAlgo.Size = new System.Drawing.Size(291, 21);
            this.cmbUserCertSigAlgo.TabIndex = 29;
            // 
            // numUserCertKeySize
            // 
            this.numUserCertKeySize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numUserCertKeySize.Increment = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numUserCertKeySize.Location = new System.Drawing.Point(422, 94);
            this.numUserCertKeySize.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.numUserCertKeySize.Minimum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numUserCertKeySize.Name = "numUserCertKeySize";
            this.numUserCertKeySize.Size = new System.Drawing.Size(291, 20);
            this.numUserCertKeySize.TabIndex = 28;
            this.numUserCertKeySize.Value = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            // 
            // numUserCertValidity
            // 
            this.numUserCertValidity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numUserCertValidity.Location = new System.Drawing.Point(422, 68);
            this.numUserCertValidity.Name = "numUserCertValidity";
            this.numUserCertValidity.Size = new System.Drawing.Size(291, 20);
            this.numUserCertValidity.TabIndex = 27;
            this.numUserCertValidity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // txtUserCertSubject
            // 
            this.txtUserCertSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserCertSubject.Location = new System.Drawing.Point(422, 15);
            this.txtUserCertSubject.Name = "txtUserCertSubject";
            this.txtUserCertSubject.Size = new System.Drawing.Size(291, 20);
            this.txtUserCertSubject.TabIndex = 26;
            this.txtUserCertSubject.Text = "CN=User Cert";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(334, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Keysize";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Validity (years)";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(334, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Signature Algo.";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(334, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Cert Subject";
            // 
            // prgStartup
            // 
            this.prgStartup.Location = new System.Drawing.Point(314, 490);
            this.prgStartup.Name = "prgStartup";
            this.prgStartup.Size = new System.Drawing.Size(756, 11);
            this.prgStartup.TabIndex = 2;
            // 
            // lblStartup
            // 
            this.lblStartup.AutoSize = true;
            this.lblStartup.Location = new System.Drawing.Point(14, 487);
            this.lblStartup.Name = "lblStartup";
            this.lblStartup.Size = new System.Drawing.Size(39, 13);
            this.lblStartup.TabIndex = 14;
            this.lblStartup.Text = "startup";
            // 
            // btnSignCSR
            // 
            this.btnSignCSR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSignCSR.Location = new System.Drawing.Point(641, 118);
            this.btnSignCSR.Name = "btnSignCSR";
            this.btnSignCSR.Size = new System.Drawing.Size(72, 23);
            this.btnSignCSR.TabIndex = 49;
            this.btnSignCSR.Text = "Sign CSR";
            this.btnSignCSR.UseVisualStyleBackColor = true;
            this.btnSignCSR.Click += new System.EventHandler(this.btnSignCSR_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 511);
            this.Controls.Add(this.lblStartup);
            this.Controls.Add(this.prgStartup);
            this.Controls.Add(this.tabControlMain);
            this.MinimumSize = new System.Drawing.Size(1000, 550);
            this.Name = "frmMain";
            this.Text = "easyPKI";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabControlMain.ResumeLayout(false);
            this.tabCA.ResumeLayout(false);
            this.grpSubCA.ResumeLayout(false);
            this.grpSubCA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSubCAKeySize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSubCAValidity)).EndInit();
            this.grpRootCA.ResumeLayout(false);
            this.grpRootCA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRootCAKeySize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRootCAValidity)).EndInit();
            this.tabClients.ResumeLayout(false);
            this.tabClients.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUserCertKeySize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserCertValidity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabCA;
        private System.Windows.Forms.TabPage tabClients;
        private System.Windows.Forms.GroupBox grpRootCA;
        private System.Windows.Forms.RichTextBox rtbRootCA;
        private System.Windows.Forms.Button btnEraseRootCA;
        private System.Windows.Forms.Button btnGenerateRootCACert;
        private System.Windows.Forms.ComboBox cmbCASigAlgo;
        private System.Windows.Forms.NumericUpDown numRootCAKeySize;
        private System.Windows.Forms.NumericUpDown numRootCAValidity;
        private System.Windows.Forms.TextBox txtCASubject;
        private System.Windows.Forms.Label lblRootCAKeySize;
        private System.Windows.Forms.Label lblRootCAValidity;
        private System.Windows.Forms.Label lblSigAlgo;
        private System.Windows.Forms.Label lblRootCASubject;
        private System.Windows.Forms.GroupBox grpSubCA;
        private System.Windows.Forms.RichTextBox rtbSubCA;
        private System.Windows.Forms.Button btnDeleteSubCA;
        private System.Windows.Forms.Button btnGenerateSubCACert;
        private System.Windows.Forms.ComboBox cmbSubCAASigAlgo;
        private System.Windows.Forms.NumericUpDown numSubCAKeySize;
        private System.Windows.Forms.NumericUpDown numSubCAValidity;
        private System.Windows.Forms.TextBox txtSubCASubject;
        private System.Windows.Forms.Label lblSubCAKeySize;
        private System.Windows.Forms.Label lblSubCAValidity;
        private System.Windows.Forms.Label lblSubCASigAlgo;
        private System.Windows.Forms.Label lblSubCASubject;
        private System.Windows.Forms.TreeView tvSubCAs;
        private System.Windows.Forms.Button btnExportRootCACert;
        private System.Windows.Forms.Button btnExportRootCAPVK;
        private System.Windows.Forms.ProgressBar prgStartup;
        private System.Windows.Forms.Label lblStartup;
        private System.Windows.Forms.Button btnExportCACert;
        private System.Windows.Forms.Button btnExportSubCAPVK;
        private System.Windows.Forms.RichTextBox rtbClientCert;
        private System.Windows.Forms.TreeView tvClientCA;
        private System.Windows.Forms.ComboBox cmbUserCertSigAlgo;
        private System.Windows.Forms.NumericUpDown numUserCertKeySize;
        private System.Windows.Forms.NumericUpDown numUserCertValidity;
        private System.Windows.Forms.TextBox txtUserCertSubject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstClientCerts;
        private System.Windows.Forms.ListBox lstClientAttributes;
        private System.Windows.Forms.Button btnDeleteAttribute;
        private System.Windows.Forms.ComboBox cmbAttributeValue;
        private System.Windows.Forms.Button btnAddAttribute;
        private System.Windows.Forms.ComboBox cmbAttribute;
        private System.Windows.Forms.RichTextBox rtbClientPrivateKey;
        private System.Windows.Forms.Button btnExportCertChainPfx;
        private System.Windows.Forms.Button btnExportCertPfx;
        private System.Windows.Forms.Button btnExportCertChain;
        private System.Windows.Forms.Button btnExportCert;
        private System.Windows.Forms.Button btnExportCertPVK;
        private System.Windows.Forms.Button btnLoadServerTemplate;
        private System.Windows.Forms.Button btnLoadClientTemplate;
        private System.Windows.Forms.Button btnDeleteCert;
        private System.Windows.Forms.Button btnCreateCert;
        private System.Windows.Forms.Button btnSignCSR;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

