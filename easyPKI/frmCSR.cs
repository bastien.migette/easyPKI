﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyPKI {
    public partial class frmCSR : Form {
        public string CSRContent = "";
        public frmCSR() {
            InitializeComponent();
        }

        private void btnSign_Click(object sender, EventArgs e) {
            CSRContent = rtbCSR.Text;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
