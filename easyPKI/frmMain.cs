﻿using easyPKI.core;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.X509;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyPKI {
    public partial class frmMain : Form {
        List<CertTreeEntry> CertTree = new List<CertTreeEntry>();
        private const string CertFolder = "certs\\";
        private const string DefaultSigAlgo = "SHA256withRSA";

        bool subCASelectIgnoreFlag = false;

        public frmMain() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            /*  CertKeyPair root = certUtil.createRootCACert("CN=test root CA", DateTime.Now.AddYears(10), new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>());
              CertKeyPair subca = certUtil.createIntermediateCACert("CN=test sub CA 1", DateTime.Now.AddYears(10), root, new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>());
              CertKeyPair userroot = certUtil.createUserCert("CN=test root user", DateTime.Now.AddYears(10), root, new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>());
              CertKeyPair serverroot = certUtil.createServerCert("CN=test root user", DateTime.Now.AddYears(10), root, new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>());

              certUtil.saveCertAndKeyToFolder(root, "root");
              certUtil.saveCertAndKeyToFolder(subca, "subca");
              certUtil.saveCertAndKeyToFolder(userroot, "userroot");
              certUtil.saveCertAndKeyToFolder(serverroot, "serverroot");
              */
        }


        private void loadCertTree() {
            CertTree.Clear();
            lblStartup.Text = "Loading Certs...";
            int countLoaded = 0;
            if (!Directory.Exists(CertFolder)) {
                return;
            }
            if (File.Exists(CertFolder + "root.cer")) {
                CertKeyPair ckp = certUtil.loadCKPFromFiles(CertFolder + "root.cer", CertFolder + "root.key");
                CertTree.Add(new CertTreeEntry(ckp, CertFolder, false, true));
                rtbRootCA.Text = certUtil.getCertPem(ckp.cert);
                loadDirectoryRecursive(CertFolder, CertTree[0], countLoaded);
                setRootCAUIState(false);
            }
        }
        private void loadDirectoryRecursive(string Currentdir, CertTreeEntry currentCertNode, int totalCount) {
            CertTreeEntry cte = currentCertNode;
            if (File.Exists(Currentdir + "CA.cer")) {
                CertKeyPair ckp = certUtil.loadCKPFromFiles(Currentdir + "CA.cer", Currentdir + "CA.key");
                cte = new CertTreeEntry(ckp, Currentdir, false, true);
                cte.Parent = currentCertNode;
                CertTree.Add(cte);
                totalCount++;
            }
            //Loading Clients
            foreach (string file in Directory.GetFiles(Currentdir, "*.cer")) {
                if (!Path.GetFileName(file).Contains("CA") && !Path.GetFileName(file).Contains("root") && !Path.GetFileName(file).Contains("chain")) {
                    CertKeyPair clientcert = certUtil.loadCKPFromFiles(Currentdir + Path.GetFileName(file), Currentdir + Path.GetFileName(file).Replace("cer", "key"));
                    CertTreeEntry ctec = new CertTreeEntry(clientcert, Currentdir, true, false);
                    ctec.baseFileName = Path.GetFileNameWithoutExtension(file);
                    ctec.Parent = cte;
                    CertTree.Add(ctec);
                    totalCount++;
                }

                lblStartup.Text = "Loading Certs... " + totalCount;
                Application.DoEvents();

            }
            foreach (string dir in Directory.GetDirectories(Currentdir)) {
                loadDirectoryRecursive(dir + "\\", cte, totalCount);
            }


        }
        private void updateUIFromTree() {
            tvSubCAs.Nodes.Clear();
            tvClientCA.Nodes.Clear();
            lstClientCerts.Items.Clear();
            lstClientAttributes.Items.Clear();
            if (CertTree.Count < 1) { return; }
            TreeNode lvi = new TreeNode();
            lvi.Text = CertTree[0].certKeyPair.cert.SubjectDN.ToString();
            lvi.Tag = CertTree[0];
            addTreeNodesRecursive(lvi, CertTree[0]);
            tvSubCAs.Nodes.Add(lvi);
            tvSubCAs.ExpandAll();
            tvClientCA.Nodes.Add((TreeNode)lvi.Clone());
            tvClientCA.ExpandAll();
        }
        public void addTreeNodesRecursive(TreeNode tn, CertTreeEntry cte) {
            foreach (CertTreeEntry child in cte.Children) {
                if (child.CACert) {
                    TreeNode newNode = tn.Nodes.Add(child.certKeyPair.cert.SubjectDN.ToString());
                    newNode.Tag = child;
                    if (child.hasChildren) {
                        addTreeNodesRecursive(newNode, child);
                    }
                }
            }

        }
        private void frmMain_Load(object sender, EventArgs e) {
            prgStartup.Style = ProgressBarStyle.Marquee;
            prgStartup.MarqueeAnimationSpeed = 30;

            foreach (string algo in certUtil.getSupportedSignatureAlgorithms()) {
                if ((algo.ToUpper().Contains("RSA") || algo.ToUpper().Contains("DSA")) && !algo.ToUpper().Contains("ECDSA")) {
                    cmbCASigAlgo.Items.Add(algo);
                    cmbSubCAASigAlgo.Items.Add(algo);
                    cmbUserCertSigAlgo.Items.Add(algo);
                }

            }
            cmbCASigAlgo.Text = DefaultSigAlgo;
            cmbSubCAASigAlgo.Text = DefaultSigAlgo;
            cmbUserCertSigAlgo.Text = DefaultSigAlgo;

            loadCertTree();
            updateUIFromTree();
            prgStartup.Style = ProgressBarStyle.Continuous;
            prgStartup.Visible = false;
            lblStartup.Visible = false;
        }
        private void setRootCAUIState(bool enabled) {
            btnGenerateRootCACert.Enabled = enabled;
            txtCASubject.Enabled = enabled;
            numRootCAKeySize.Enabled = enabled;
            numRootCAValidity.Enabled = enabled;
            cmbCASigAlgo.Enabled = enabled;
        }
        private void btnGenerateRootCACert_Click(object sender, EventArgs e) {
            try {
                setRootCAUIState(false);
                if (!Directory.Exists(CertFolder)) {
                    Directory.CreateDirectory(CertFolder);
                }
                CertKeyPair root = certUtil.createRootCACert(txtCASubject.Text, DateTime.Now.AddYears((int)numRootCAValidity.Value),
                    new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>(), cmbCASigAlgo.Text, (int)numRootCAKeySize.Value);
                CertTree = new List<CertTreeEntry>();
                CertTree.Add(new CertTreeEntry(root, CertFolder, true, true));
                certUtil.saveCertAndKeyToFolder(root, "root", CertFolder, "", false);
                rtbRootCA.Text = certUtil.getCertPem(root.cert);
                updateUIFromTree();

            } catch (Exception E) {
                MessageBox.Show("An error has occured: \n" + E.Message);
                setRootCAUIState(true);
            }
        }

        public List<CertKeyPair> getCertChain(CertKeyPair cert) {
            List<CertKeyPair> r = new List<CertKeyPair>();
            foreach (CertTreeEntry cte in CertTree) {
                if (cte.certKeyPair.cert == cert.cert) {
                    r.Add(cte.certKeyPair);
                    CertTreeEntry current = cte;
                    while (current.Parent != null) {
                        r.Insert(0, current.Parent.certKeyPair);
                        current = current.Parent;
                    }
                    r.Reverse();
                    return r;
                }
            }
            return r;
        }

        private void btnEraseRootCA_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Deleting root CA will delete all SubCA and certs, are you sure ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                try {
                    Directory.Delete(CertFolder, true);
                    loadCertTree();
                    updateUIFromTree();
                    setRootCAUIState(true);
                } catch (Exception E) {
                    MessageBox.Show("An error has occured: \n" + E.Message);
                }
            }
        }

        private void selectSubCA(CertTreeEntry cte) {
            subCASelectIgnoreFlag = true;
            foreach (TreeNode tn in tvSubCAs.Nodes) {
                if ((CertTreeEntry)tn.Tag == cte) {
                    tvSubCAs.SelectedNode = tn;
                    return;
                }
            }
            subCASelectIgnoreFlag = false;
        }

        private void btnGenerateSubCACert_Click(object sender, EventArgs e) {
            if (tvSubCAs.SelectedNode == null) {
                MessageBox.Show("Please select a parent CA first !");
                return;
            }

            try {
                btnGenerateSubCACert.Enabled = false;
                CertTreeEntry parent = (CertTreeEntry)tvSubCAs.SelectedNode.Tag;
                string folder = parent.folder + parent.Children.Count() + "\\";
                Directory.CreateDirectory(parent.folder + parent.Children.Count() + "\\");

                CertKeyPair subca = certUtil.createIntermediateCACert(txtSubCASubject.Text, DateTime.Now.AddYears((int)numSubCAValidity.Value), parent.certKeyPair,
                    new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>(), cmbSubCAASigAlgo.Text, (int)numSubCAKeySize.Value);


                CertTreeEntry ctSubCA = new CertTreeEntry(subca, folder, true, true);
                ctSubCA.Parent = parent;
                CertTree.Add(ctSubCA);
                certUtil.saveCertAndKeyToFolder(subca, "CA", folder, "", false);
                rtbSubCA.Text = certUtil.getCertPem(subca.cert);
                updateUIFromTree();
                selectSubCA(ctSubCA);

            } catch (Exception E) {
                MessageBox.Show("An error has occured: \n" + E.Message);
            }
            btnGenerateSubCACert.Enabled = true;

        }

        private void tvSubCAs_AfterSelect(object sender, TreeViewEventArgs e) {
            if (subCASelectIgnoreFlag) { return; }
            CertTreeEntry subca = (CertTreeEntry)tvSubCAs.SelectedNode.Tag;
            txtSubCASubject.Text = subca.certKeyPair.cert.SubjectDN.ToString();
            numSubCAValidity.Value = (decimal)Math.Round((subca.certKeyPair.cert.NotAfter - subca.certKeyPair.cert.NotBefore).TotalDays / 365, 0);

            numSubCAKeySize.Value = certUtil.getCertKeySize(subca.certKeyPair.cert);
            cmbSubCAASigAlgo.Text = subca.certKeyPair.cert.SigAlgName.Replace("-", "").ToUpper();
            rtbSubCA.Text = certUtil.getCertPem(subca.certKeyPair.cert);

        }

        private void btnDeleteSubCA_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Deleting Sub CA will delete all child SubCA and certs, are you sure ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                try {
                    CertTreeEntry subca = (CertTreeEntry)tvSubCAs.SelectedNode.Tag;
                    Directory.Delete(subca.folder, true);
                    loadCertTree();
                    updateUIFromTree();
                    setRootCAUIState(true);
                } catch (Exception E) {
                    MessageBox.Show("An error has occured: \n" + E.Message);
                }
            }
        }

        private List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> ParseCertAttributes() {
            List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> r = new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>();
            //GeneralNames subjectAltName = new GeneralNames(new GeneralName(GeneralName.iPAddress, getCommonName(csr)));
            //GeneralNames subjectAltName = new GeneralNames(new GeneralName(GeneralName.DnsName, "example@example.org"));
            int ku = 0;
            List<KeyPurposeID> ekus = new List<KeyPurposeID>();
            List<GeneralName> SANs = new List<GeneralName>();
            foreach (string itm in lstClientAttributes.Items) {
                string[] op = itm.Split('=');
                if (op[0] == "Key Usage") {
                    ku |= certUtil.getKeyUsageFromString(op[1]);
                } else if (op[0] == "Extended Key Usage") {
                    ekus.Add(certUtil.getExtKeyUsageFromString(op[1]));
                } else if (op[0] == "SAN - DNS") {
                    SANs.Add(new GeneralName(GeneralName.DnsName, op[1]));
                } else if (op[0] == "SAN - IP") {
                    SANs.Add(new GeneralName(GeneralName.IPAddress, op[1]));
                }
            }
            if (SANs.Count() > 0) {
                r.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.SubjectAlternativeName, new GeneralNames(SANs.ToArray())));
            }
            if (ku > 0) {
                r.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.KeyUsage, new KeyUsage(ku)));
            }

            if (ekus.Count() > 0) {
                r.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.ExtendedKeyUsage, new ExtendedKeyUsage(ekus.ToArray())));
            }

            return r;
        }
        private void btnCreateCert_Click(object sender, EventArgs e) {
            List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions = ParseCertAttributes();
            if (tvClientCA.SelectedNode == null) {
                MessageBox.Show("Please select a parent CA first !");
                return;
            }

            try {
                btnCreateCert.Enabled = false;
                CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
                string folder = parent.folder;
                int id = parent.Children.Count();


                CertKeyPair usercert = certUtil.createUserCert(txtUserCertSubject.Text, DateTime.Now.AddYears((int)numUserCertValidity.Value), parent.certKeyPair,
                    Extensions, cmbUserCertSigAlgo.Text, (int)numUserCertKeySize.Value);


                CertTreeEntry usercerttree = new CertTreeEntry(usercert, folder, true, false);
                usercerttree.Parent = parent;
                CertTree.Add(usercerttree);
                while (File.Exists(folder + id.ToString() + ".cer")) {
                    id++;
                }
                certUtil.saveCertAndKeyToFolder(usercert, id.ToString(), folder, "", true);
                certUtil.saveCertChainAndKeyToFolder(getCertChain(usercert), id.ToString(), folder, "", true);
                updateClientCertListUI();
                loadClientCertUI(lstClientCerts.Items.Count - 1);


            } catch (Exception E) {
                MessageBox.Show("An error has occured: \n" + E.Message);
            }
            btnCreateCert.Enabled = true;
        }

        public static short[] StringToByteArray(string hex) {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToInt16(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public string hexAttr2IP(string hexstr) {
            short[] r = StringToByteArray(hexstr.Replace("#", ""));
            List<string> parts = new List<string>();
            foreach (short b in r) {
                parts.Add(b.ToString());

            }
            return String.Join(".", parts);

        }

        public void loadClientCertUI(int id) {
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertKeyPair usercert = parent.Children[id].certKeyPair;

            rtbClientCert.Text = certUtil.getCertPem(usercert.cert);
            rtbClientPrivateKey.Text = certUtil.getKeyPem(usercert.keyPair.Private);
            txtUserCertSubject.Text = usercert.cert.SubjectDN.ToString();
            numUserCertKeySize.Value = certUtil.getCertKeySize(usercert.cert);
            cmbUserCertSigAlgo.Text = usercert.cert.SigAlgName.Replace("-", "").ToUpper();
            lstClientAttributes.Items.Clear();
            //https://github.com/bcgit/bc-csharp/blob/master/crypto/src/x509/X509Certificate.cs
            if (usercert.cert.GetSubjectAlternativeNames() != null) {
                foreach (ArrayList san in usercert.cert.GetSubjectAlternativeNames()) {
                    if ((int)san[0] == GeneralName.DnsName) {
                        lstClientAttributes.Items.Add("SAN - DNS=" + san[1]);
                    } else {
                        lstClientAttributes.Items.Add("SAN - IP=" + hexAttr2IP((string)san[1]));

                    }
                }
            }
            bool[] kus = usercert.cert.GetKeyUsage();
            /*      KeyUsage ::= BIT STRING {
           digitalSignature        (0),
           nonRepudiation          (1),
           keyEncipherment         (2),
           dataEncipherment        (3),
           keyAgreement            (4),
           keyCertSign             (5),
           cRLSign                 (6),
           encipherOnly            (7),
           decipherOnly            (8) }
           */
            if (kus[0]) {
                lstClientAttributes.Items.Add("Key Usage=DigitalSignature");
            }
            if (kus[1]) {
                lstClientAttributes.Items.Add("Key Usage=NonRepudiation");
            }
            if (kus[2]) {
                lstClientAttributes.Items.Add("Key Usage=KeyEncipherment");
            }
            if (kus[3]) {
                lstClientAttributes.Items.Add("Key Usage=DataEncipherment");
            }
            if (kus[4]) {
                lstClientAttributes.Items.Add("Key Usage=KeyAgreement");
            }
            if (kus[5]) {
                lstClientAttributes.Items.Add("Key Usage=KeyCertSign");
            }
            if (kus[6]) {
                lstClientAttributes.Items.Add("Key Usage=CrlSign");
            }
            if (kus[7]) {
                lstClientAttributes.Items.Add("Key Usage=EncipherOnly");
            }
            if (kus[8]) {
                lstClientAttributes.Items.Add("Key Usage=DecipherOnly");
            }
            if (usercert.cert.GetExtendedKeyUsage() != null) {
                foreach (string eku in usercert.cert.GetExtendedKeyUsage()) {
                    lstClientAttributes.Items.Add("Extended Key Usage=" + certUtil.getEKUFromOID(eku));
                }
            }
        }


        private void updateClientCertListUI() {
            if (tvClientCA.SelectedNode == null) {
                MessageBox.Show("Please select a parent CA first !");
                return;
            }
            lstClientCerts.Items.Clear();
            lstClientAttributes.Items.Clear();
            foreach (CertTreeEntry cte in ((CertTreeEntry)tvClientCA.SelectedNode.Tag).Children) {
                if (cte.lastCert) {
                    lstClientCerts.Items.Add(cte.certKeyPair.cert.SubjectDN);
                }

            }
        }

        private void cmbAttribute_SelectedIndexChanged(object sender, EventArgs e) {
            if (cmbAttribute.Text == "Key Usage") {
                cmbAttributeValue.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbAttributeValue.Items.Clear();
                cmbAttributeValue.Items.Add("EncipherOnly");
                cmbAttributeValue.Items.Add("CrlSign");
                cmbAttributeValue.Items.Add("KeyCertSign");
                cmbAttributeValue.Items.Add("KeyAgreement");
                cmbAttributeValue.Items.Add("DataEncipherment");
                cmbAttributeValue.Items.Add("KeyEncipherment");
                cmbAttributeValue.Items.Add("NonRepudiation");
                cmbAttributeValue.Items.Add("DigitalSignature");
                cmbAttributeValue.Items.Add("DecipherOnly");

            } else if (cmbAttribute.Text == "Extended Key Usage") {
                cmbAttributeValue.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbAttributeValue.Items.Clear();
                cmbAttributeValue.Items.Add("AnyExtendedKeyUsage");
                cmbAttributeValue.Items.Add("IdKPClientAuth");
                cmbAttributeValue.Items.Add("IdKPCodeSigning");
                cmbAttributeValue.Items.Add("IdKPEmailProtection");
                cmbAttributeValue.Items.Add("IdKPIpsecEndSystem");
                cmbAttributeValue.Items.Add("IdKPIpsecTunnel");
                cmbAttributeValue.Items.Add("IdKPIpsecUser");
                cmbAttributeValue.Items.Add("IdKPOcspSigning");
                cmbAttributeValue.Items.Add("IdKPServerAuth");
                cmbAttributeValue.Items.Add("IdKPSmartCardLogon");
                cmbAttributeValue.Items.Add("IdKPTimeStamping");

            } else {
                cmbAttributeValue.DropDownStyle = ComboBoxStyle.DropDown;
            }

        }

        private void btnAddAttribute_Click(object sender, EventArgs e) {
            lstClientAttributes.Items.Add(cmbAttribute.Text + "=" + cmbAttributeValue.Text);
        }

        private void btnDeleteAttribute_Click(object sender, EventArgs e) {
            if (!(lstClientAttributes.SelectedIndex >= 0)) {
                MessageBox.Show("Please select attribute first");
                return;
            }
            lstClientAttributes.Items.RemoveAt(lstClientAttributes.SelectedIndex);
        }

        private void btnLoadClientTemplate_Click(object sender, EventArgs e) {
            lstClientAttributes.Items.Clear();
            lstClientAttributes.Items.Add("Key Usage=DigitalSignature");
            lstClientAttributes.Items.Add("Key Usage=KeyEncipherment");
            lstClientAttributes.Items.Add("Extended Key Usage=IdKPClientAuth");
        }

        private void btnLoadServerTemplate_Click(object sender, EventArgs e) {
            lstClientAttributes.Items.Clear();
            lstClientAttributes.Items.Add("Key Usage=DigitalSignature");
            lstClientAttributes.Items.Add("Key Usage=KeyEncipherment");
            lstClientAttributes.Items.Add("Extended Key Usage=IdKPClientAuth");
            lstClientAttributes.Items.Add("Extended Key Usage=IdKPServerAuth");
        }

        private void tvClientCA_AfterSelect(object sender, TreeViewEventArgs e) {
            CertTreeEntry subca = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            numUserCertKeySize.Value = certUtil.getCertKeySize(subca.certKeyPair.cert);
            cmbUserCertSigAlgo.Text = subca.certKeyPair.cert.SigAlgName.Replace("-", "").ToUpper();
            lstClientAttributes.Items.Clear();
            rtbClientCert.Text = "";
            rtbClientPrivateKey.Text = "";
            updateClientCertListUI();

        }

        private void lstClientCerts_SelectedIndexChanged(object sender, EventArgs e) {
            loadClientCertUI(lstClientCerts.SelectedIndex);
        }

        private void btnDeleteCert_Click(object sender, EventArgs e) {

            if (lstClientCerts.SelectedIndex < 0) {
                MessageBox.Show("Please select certificate first");
                return;
            }
            try {
                CertTreeEntry subca = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
                CertTreeEntry client = subca.Children[lstClientCerts.SelectedIndex];
                File.Delete(subca.folder + client.baseFileName + ".cer");
                File.Delete(subca.folder + client.baseFileName + ".key");
                File.Delete(subca.folder + client.baseFileName + ".pfx");
                File.Delete(subca.folder + client.baseFileName + "_chain.pfx");
                File.Delete(subca.folder + client.baseFileName + "_chain.cer");

                subca.Children.RemoveAt(lstClientCerts.SelectedIndex);
                CertTree.Remove(client);
                updateClientCertListUI();


            } catch (Exception E) {
                MessageBox.Show("An error has occured: \n" + E.Message);
            }


        }

        private void btnSignCSR_Click(object sender, EventArgs e) {
            if (tvClientCA.SelectedNode == null) {
                MessageBox.Show("Please select a parent CA first !");
                return;
            }
            CertTreeEntry subca = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            frmCSR fcsr = new frmCSR(); 
            fcsr.ShowDialog();
            try {
                btnSignCSR.Enabled = false;
                List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions = ParseCertAttributes();
                if (fcsr.DialogResult == DialogResult.OK) {
                    X509Certificate cert = certUtil.signCSR(fcsr.CSRContent, subca.certKeyPair, DateTime.Now.AddYears((int)numUserCertValidity.Value), Extensions, cmbUserCertSigAlgo.Text);

                    saveFileDialog.Filter = "Certificate File (.pem)|*.pem";
                    saveFileDialog.ShowDialog();

                    TextWriter textWriter = new StreamWriter(saveFileDialog.FileName,true);
                    PemWriter pemWriter = new PemWriter(textWriter);

                    pemWriter.WriteObject(cert);
                    pemWriter.Writer.Flush();

                    textWriter.Close();

                     textWriter = new StreamWriter(saveFileDialog.FileName.Replace(".pem","_chain.pem"));
                     pemWriter = new PemWriter(textWriter);
                    pemWriter.WriteObject(cert);
                    foreach(CertKeyPair kp in getCertChain(subca.certKeyPair)) {
                        pemWriter.WriteObject(kp.cert);
                    }
                    pemWriter.Writer.Flush();

                    textWriter.Close();
                }

            } catch (Exception E) {
                MessageBox.Show("An error has occured: \n" + E.Message);
            }
            btnSignCSR.Enabled = true;
        }

        private void btnExportCertPVK_Click(object sender, EventArgs e) {
            int id = lstClientCerts.SelectedIndex;
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertTreeEntry usercert = parent.Children[id];

            saveFileDialog.Filter = "Private key file|*.key";
            saveFileDialog.ShowDialog();
            File.Copy(usercert.folder+usercert.baseFileName+".key", saveFileDialog.FileName,true);
        }

        private void btnExportCert_Click(object sender, EventArgs e) {
            int id = lstClientCerts.SelectedIndex;
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertTreeEntry usercert = parent.Children[id];

            saveFileDialog.Filter = "Certificate File|*.cer";
            saveFileDialog.ShowDialog();
            File.Copy(usercert.folder + usercert.baseFileName + ".cer", saveFileDialog.FileName,true);
        }

        private void btnExportCertChain_Click(object sender, EventArgs e) {
            int id = lstClientCerts.SelectedIndex;
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertTreeEntry usercert = parent.Children[id];

            saveFileDialog.Filter = "Certificate File|*.cer";
            saveFileDialog.ShowDialog();
            File.Copy(usercert.folder + usercert.baseFileName + "_chain.cer", saveFileDialog.FileName,true);
        }

        private void btnExportCertPfx_Click(object sender, EventArgs e) {
            int id = lstClientCerts.SelectedIndex;
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertTreeEntry usercert = parent.Children[id];

            saveFileDialog.Filter = "Certificate File (pfx)|*.pfx";
            saveFileDialog.ShowDialog();
            File.Copy(usercert.folder + usercert.baseFileName + ".pfx", saveFileDialog.FileName,true);
        }

        private void btnExportCertChainPfx_Click(object sender, EventArgs e) {
            int id = lstClientCerts.SelectedIndex;
            if (tvClientCA.SelectedNode == null || id < 0 || id > lstClientCerts.Items.Count - 1) {
                return;
            }
            CertTreeEntry parent = (CertTreeEntry)tvClientCA.SelectedNode.Tag;
            CertTreeEntry usercert = parent.Children[id];

            saveFileDialog.Filter = "Certificate File (pfx)|*.pfx";
            saveFileDialog.ShowDialog();
            File.Copy(usercert.folder + usercert.baseFileName + "_chain.pfx", saveFileDialog.FileName,true);
        }

        private void btnExportRootCAPVK_Click(object sender, EventArgs e) {
            saveFileDialog.Filter = "Private key file|*.key";
            saveFileDialog.ShowDialog();
            File.Copy(CertFolder + "root.key", saveFileDialog.FileName,true);
        }

        private void btnExportRootCACert_Click(object sender, EventArgs e) {
            saveFileDialog.Filter = "Certificate File|*.cer";
            saveFileDialog.ShowDialog();
            File.Copy(CertFolder + "root.cer", saveFileDialog.FileName,true);
        }

        private void button2_Click(object sender, EventArgs e) {

        }

        private void btnExportSubCAPVK_Click(object sender, EventArgs e) {
            if (subCASelectIgnoreFlag) { return; }
            CertTreeEntry subca = (CertTreeEntry)tvSubCAs.SelectedNode.Tag;
            saveFileDialog.Filter = "Private key file|*.key";
            saveFileDialog.ShowDialog();
            File.Copy(subca.folder + "CA.key", saveFileDialog.FileName,true);
        }

        private void btnExportCACert_Click(object sender, EventArgs e) {
            if (subCASelectIgnoreFlag) { return; }
            CertTreeEntry subca = (CertTreeEntry)tvSubCAs.SelectedNode.Tag;
            saveFileDialog.Filter = "Certificate File|*.cer";
            saveFileDialog.ShowDialog();
            File.Copy(subca.folder + "CA.cer", saveFileDialog.FileName,true);
        }
    }
}
