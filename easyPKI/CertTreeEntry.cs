﻿using easyPKI.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace easyPKI {
    public class CertTreeEntry {
        private CertTreeEntry _parent = null;
        private List<CertTreeEntry> _children = new List<CertTreeEntry>();
        public string folder;
        public bool lastCert = true;
        public bool CACert = false;
        public CertKeyPair certKeyPair;
        public string baseFileName = "";
        public bool hasChildren {
            get {
                return (_children.Count > 0);
            }
        }
        public CertTreeEntry Parent {
            get {
                return _parent;
            }
            set {
                value._children.Add(this);
                _parent = value;
                value.lastCert = false;
            }
        }
        public List<CertTreeEntry> Children {
            get {
                return _children;
            }
            set {
                _children = value;
                foreach (CertTreeEntry c in value) {
                    c._parent = this;
                }
                if (_children.Count > 0) {
                    lastCert = false;
                } else {
                    lastCert = true;
                }
            }
        }
        
        public CertTreeEntry(CertKeyPair certKeyPair, string folder, bool lastCert = true, bool CACert = false) {
            this.certKeyPair = certKeyPair;
            this.folder = folder;
            this.CACert = CACert;
            this.lastCert = lastCert;
        }
    }
}
