﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.X509.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace easyPKI.core {
    public static class certUtil {
        static BigInteger LOWESTSN = new BigInteger("0080000000000000", 16);
        static BigInteger HIGHESTSN = new BigInteger("7FFFFFFFFFFFFFFF", 16);
        public static SecureRandom rand = new SecureRandom();



        public static BigInteger GenerateSerialNumber() {

            byte[] sernobytes = new byte[8];
            bool ok = false;
            BigInteger serno = null;
            while (!ok) {
                rand.NextBytes(sernobytes);
                serno = new BigInteger(sernobytes).Abs();

                // Must be within the range 0080000000000000 - 7FFFFFFFFFFFFFFF
                if ((serno.CompareTo(LOWESTSN) >= 0) && (serno.CompareTo(HIGHESTSN) <= 0)) {
                    ok = true;
                }
            }
            return serno;
        }




        /// <summary>
        /// Creates an SubjectKeyIdentifier based on a public key
        /// </summary>
        /// <param name="publicKey">The public key from which to generate the result</param>
        /// <returns>A SubjectKeyIdentifier instance</returns>
        /// <exception cref="Exception">Will be thrown if the key is not public</exception>
        public static SubjectKeyIdentifier CreateSubjectKeyId(AsymmetricKeyParameter publicKey) {
            SubjectPublicKeyInfo _info = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey);
            return new SubjectKeyIdentifier(_info);
        }

        private static bool hasExtension(DerObjectIdentifier id, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions) {
            foreach (KeyValuePair<DerObjectIdentifier, Asn1Encodable> kvext in Extensions) {
                if (kvext.Key.Id == id.Id) {
                    return true;
                }
            }
            return false;
        }


        public static AsymmetricCipherKeyPair ReadPemPvk(String filename) {
            StreamReader sR = new StreamReader(filename);
            PemReader pR = new PemReader(sR);

            AsymmetricCipherKeyPair k = (AsymmetricCipherKeyPair)pR.ReadObject();

            sR.Close();
            return k;
        }


        public static CmsSignedData ReadPem(String filename) {
            StreamReader sR = new StreamReader(filename);
            PemReader pR = new PemReader(sR);

            Org.BouncyCastle.Asn1.Cms.ContentInfo cI = (Org.BouncyCastle.Asn1.Cms.ContentInfo)pR.ReadObject();

            sR.Close();

            CmsSignedData cms = new CmsSignedData(cI);

            return cms;
        }

        public static X509Certificate ReadPemX509(String filename) {
            StreamReader sR = new StreamReader(filename);
            PemReader pR = new PemReader(sR);

            X509Certificate x = (X509Certificate)pR.ReadObject();
            sR.Close();
            return x;

        }

        public static CertKeyPair loadCKPFromFiles(string crt, string pvk) {

            X509Certificate cert = ReadPemX509(crt);
            AsymmetricCipherKeyPair csd = ReadPemPvk(pvk);
            AsymmetricCipherKeyPair kp = new AsymmetricCipherKeyPair(cert.GetPublicKey(), (AsymmetricKeyParameter)csd.Private);
            CertKeyPair ckp = new CertKeyPair(cert, kp);
            return ckp;
        }

        /// <summary>
        /// Function to create intermediate CA using createSignedCert
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="ValidUntil"></param>
        /// <param name="issuer"></param>
        /// <param name="Extensions"></param>
        /// <param name="SigAlgo"></param>
        /// <param name="keysize"></param>
        /// <returns></returns>
        public static CertKeyPair createIntermediateCACert(string subject, DateTime ValidUntil, CertKeyPair issuer, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.BasicConstraints, new BasicConstraints(true))); //Not setting limit to support any Chain size
            }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.KeyUsage, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.CrlSign | KeyUsage.KeyCertSign)));
            }
            return createSignedCert(subject, ValidUntil, issuer, Extensions, SigAlgo, keysize);
        }

        /// <summary>
        /// Function to create end user using createSignedCert. Will default key usage to DigitalSignature + KeyEncipherment and EKU to client auth
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="ValidUntil"></param>
        /// <param name="issuer"></param>
        /// <param name="Extensions"></param>
        /// <param name="SigAlgo"></param>
        /// <param name="keysize"></param>
        /// <returns></returns>
        public static CertKeyPair createUserCert(string subject, DateTime ValidUntil, CertKeyPair issuer, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.BasicConstraints, new BasicConstraints(false)));
            }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.KeyUsage, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.KeyEncipherment)));
            }
            if (!hasExtension(X509Extensions.ExtendedKeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.ExtendedKeyUsage, new ExtendedKeyUsage(new KeyPurposeID[] { KeyPurposeID.IdKPClientAuth })));
            }

            return createSignedCert(subject, ValidUntil, issuer, Extensions, SigAlgo, keysize);
        }

        /// <summary>
        /// Function to create end user using createSignedCert. Will default key usage to DigitalSignature + KeyEncipherment and EKU to client auth + server auth
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="ValidUntil"></param>
        /// <param name="issuer"></param>
        /// <param name="Extensions"></param>
        /// <param name="SigAlgo"></param>
        /// <param name="keysize"></param>
        /// <returns></returns>
        public static CertKeyPair createServerCert(string subject, DateTime ValidUntil, CertKeyPair issuer, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.BasicConstraints, new BasicConstraints(false)));
            }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.KeyUsage, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.KeyEncipherment)));
            }
            if (!hasExtension(X509Extensions.ExtendedKeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.ExtendedKeyUsage, new ExtendedKeyUsage(new KeyPurposeID[] { KeyPurposeID.IdKPClientAuth, KeyPurposeID.IdKPServerAuth })));
            }

            return createSignedCert(subject, ValidUntil, issuer, Extensions, SigAlgo, keysize);
        }


        public static AsymmetricCipherKeyPair getKeyPairForAlgo(string algo, int keysize) {
            AsymmetricCipherKeyPair KP;
            if (algo.ToUpper().Contains("RSA")) {
                var kpg = GeneratorUtilities.GetKeyPairGenerator("RSA"); 
                kpg.Init(new RsaKeyGenerationParameters(BigInteger.ValueOf(17), rand, keysize, 25));
                KP = kpg.GenerateKeyPair();
                /* } else if (algo.ToUpper().Contains("EDSA")) {
                     var kpg = GeneratorUtilities.GetKeyPairGenerator("ECDSA");
                     ECCurve curve = new ECCurve.Fp(
                 new BigInteger("883423532389192164791648750360308885314476597252960362792450860609699839"), // q
                 new BigInteger("7fffffffffffffffffffffff7fffffffffff8000000000007ffffffffffc", 16), // a
                 new BigInteger("6b016c3bdcf18941d0d654921475ca71a9db2fb27d1d37796185c2942c0a", 16)); // b
                     kpg.Init(new ECKeyGenerationParameters(new ECDomainParameters(), rand));
                     KP = kpg.GenerateKeyPair();*/
                //http://bouncy-castle.1462172.n4.nabble.com/Signature-wrong-length-using-ECDSA-using-P-521-td4658010.html
            } else {//dsa
              
                DsaKeyPairGenerator a = new DsaKeyPairGenerator();

                //DSA Key Parameter Generator
                DsaParametersGenerator paramgen = new DsaParametersGenerator();

                //Initialize Key Parameter Generator
                if (keysize > 1024) {
                    keysize = 1024;
                }
                paramgen.Init(keysize, 100, new SecureRandom());
                //DSA KeyGeneration Parameters 
                DsaKeyGenerationParameters param = new DsaKeyGenerationParameters(new SecureRandom(), paramgen.GenerateParameters());

                //Initialize the Key Pair Generator
                a.Init(param);
                KP = a.GenerateKeyPair();
            }


            return KP;
        }
        /// <summary>
        /// Generic function to create signed cert. It might miss some extensions in order to be compatible, depending on use case.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="ValidUntil"></param>
        /// <param name="issuer"></param>
        /// <param name="Extensions"></param>
        /// <param name="SigAlgo"></param>
        /// <param name="keysize"></param>
        /// <returns></returns>
        public static CertKeyPair createSignedCert(string subject, DateTime ValidUntil, CertKeyPair issuer, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
          
            AsymmetricCipherKeyPair subjectKeyPair = getKeyPairForAlgo(SigAlgo, keysize);

            v3CertGen.Reset();

            v3CertGen.SetSerialNumber(GenerateSerialNumber()); 
            // In this case the issuersCert.SubjectDN and issuersCert.IssuersDN 
            // are NOT the same and the subject is the correct one to use
            v3CertGen.SetIssuerDN(issuer.cert.SubjectDN);
            v3CertGen.SetNotBefore(DateTime.UtcNow);
            v3CertGen.SetNotAfter(ValidUntil);
            v3CertGen.SetSubjectDN(new X509Name(subject, new PrintableStringEntryConverter()));
            v3CertGen.SetPublicKey(subjectKeyPair.Public);



            foreach (KeyValuePair<DerObjectIdentifier, Asn1Encodable> kvext in Extensions) {
                v3CertGen.AddExtension(kvext.Key, false, kvext.Value);
            }

            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.CertificateIssuer, false, issuer.cert.SubjectDN);
            }
            if (!hasExtension(X509Extensions.SubjectKeyIdentifier, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(subjectKeyPair.Public));
            }
            if (!hasExtension(X509Extensions.AuthorityKeyIdentifier, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(issuer.keyPair.Public));
            }

            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.BasicConstraints, new BasicConstraints(false)));
            }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.KeyUsage, false, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.KeyEncipherment));
            }

            //v3CertGen.AddExtension(X509Extensions.SubjectAlternativeName, true, new SubjectAlternativeName)

            Asn1SignatureFactory sig = new Asn1SignatureFactory(SigAlgo, issuer.keyPair.Private, rand);
            X509Certificate cert = v3CertGen.Generate(sig);

            return new CertKeyPair(cert, subjectKeyPair);

        }
        //List of algorithms: https://bouncycastle.org/specifications.html
        public static CertKeyPair createRootCACert(string subject, DateTime ValidUntil, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            /*  https://www.ietf.org/rfc/rfc3280.txt
           The pathLenConstraint field is meaningful only if the cA boolean is
           asserted and the key usage extension asserts the keyCertSign bit
           (section 4.2.1.3).  In this case, it gives the maximum number of non-
           self-issued intermediate certificates that may follow this
           certificate in a valid certification path.  A certificate is self-
           issued if the DNs that appear in the subject and issuer fields are
           identical and are not empty.  (Note: The last certificate in the
           certification path is not an intermediate certificate, and is not
           included in this limit.  Usually, the last certificate is an end
           entity certificate, but it can be a CA certificate.)  A
           pathLenConstraint of zero indicates that only one more certificate
           may follow in a valid certification path.  Where it appears, the
           pathLenConstraint field MUST be greater than or equal to zero.  Where
           pathLenConstraint does not appear, no limit is imposed.
           
             BasicConstraints ::= SEQUENCE {
            cA                      BOOLEAN DEFAULT FALSE,
            pathLenConstraint       INTEGER (0..MAX) OPTIONAL }
           */
            if (!hasExtension(X509Extensions.BasicConstraints, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.BasicConstraints, new BasicConstraints(true)));
            }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                Extensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(X509Extensions.KeyUsage, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.CrlSign | KeyUsage.KeyCertSign)));
            }


            return createSelfSignedCert(subject, ValidUntil, Extensions, SigAlgo, keysize);
        }

        public static CertKeyPair createSelfSignedCert(string subject, DateTime ValidUntil, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> Extensions, string SigAlgo = "SHA256withRSA", int keysize = 2048) {
            X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();

            AsymmetricCipherKeyPair keyPair = getKeyPairForAlgo(SigAlgo, keysize);

            v3CertGen.Reset();
            v3CertGen.SetSerialNumber(GenerateSerialNumber());
            // The next two are the same in a root CA
            v3CertGen.SetIssuerDN(new X509Name(subject, new PrintableStringEntryConverter()));
            v3CertGen.SetSubjectDN(new X509Name(subject, new PrintableStringEntryConverter()));
            v3CertGen.SetPublicKey(keyPair.Public);
            v3CertGen.SetNotBefore(DateTime.UtcNow);
            v3CertGen.SetNotAfter(ValidUntil);


            foreach (KeyValuePair<DerObjectIdentifier, Asn1Encodable> kvext in Extensions) {
                v3CertGen.AddExtension(kvext.Key, false, kvext.Value);
            }


          //  if (!hasExtension(X509Extensions.CertificateIssuer, Extensions)) {
           //     v3CertGen.AddExtension(X509Extensions.CertificateIssuer, false, new DerOctetString(Encoding.UTF8.GetBytes(subject)));
           // }

            if (!hasExtension(X509Extensions.KeyUsage, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.KeyUsage, true, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.KeyEncipherment));
            }
            if (!hasExtension(X509Extensions.SubjectKeyIdentifier, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(keyPair.Public));
            }
            if (!hasExtension(X509Extensions.AuthorityKeyIdentifier, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(keyPair.Public));
            }
            /*
            if (!hasExtension(X509Extensions.ExtendedKeyUsage, Extensions)) {
                v3CertGen.AddExtension(X509Extensions.ExtendedKeyUsage, false, new ExtendedKeyUsage(new KeyPurposeID[] { KeyPurposeID.IdKPClientAuth, KeyPurposeID.IdKPServerAuth }));
            } */
            Asn1SignatureFactory sig = new Asn1SignatureFactory(SigAlgo, keyPair.Private, rand);
            X509Certificate cert = v3CertGen.Generate(sig);

            // cert.CheckValidity();
            //cert.Verify(keyPair.Public);


            return new CertKeyPair(cert, keyPair);
        }
        public static List<string> getSupportedSignatureAlgorithms() {
            List<string> r = new List<string>();
            foreach (string algo in Asn1SignatureFactory.SignatureAlgNames) {
                r.Add(algo);
            }
            return r;
        }
        public static string getCertPem(X509Certificate cert) {
            StringWriter textWriter = new StringWriter();
            PemWriter pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(cert);
            pemWriter.Writer.Flush();
            textWriter.Close();
            return textWriter.ToString();
        }

        public static string getKeyPem(AsymmetricKeyParameter key) {
            StringWriter textWriter = new StringWriter();
            PemWriter pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(key);
            pemWriter.Writer.Flush();
            textWriter.Close();
            return textWriter.ToString();
        }
        //http://javadoc.iaik.tugraz.at/iaik_jce/current/iaik/x509/extensions/ExtendedKeyUsage.html#anyExtendedKeyUsage
        public static string getEKUFromOID(string oid) {
            if (oid == "1.3.6.1.5.5.7.3.1") {
                return "IdKPServerAuth";
            }
            if (oid == "1.3.6.1.5.5.7.3.2") {
                return "IdKPClientAuth";
            }
            if (oid == "1.3.6.1.5.5.7.3.3") {
                return "IdKPCodeSigning";
            }
            if (oid == "1.3.6.1.5.5.7.3.4") {
                return "IdKPEmailProtection";
            }
            if (oid == "1.3.6.1.5.5.7.3.5") {
                return "IdKPIpsecEndSystem";
            }
            if (oid == "1.3.6.1.5.5.7.3.6") {
                return "IdKPIpsecTunnel";
            }
            if (oid == "1.3.6.1.5.5.7.3.7") {
                return "IdKPIpsecUser";
            }
            if (oid == "1.3.6.1.5.5.7.3.8") {
                return "IdKPTimeStamping";
            }
            if (oid == "1.3.6.1.5.5.7.3.9") {
                return "IdKPOcspSigning";
            }
            if (oid == "2.5.29.37.0") {
                return "AnyExtendedKeyUsage";
            }
            if (oid == "1.3.6.1.4.1.311.20.2.2") {
                return "IdKPSmartCardLogon";
            }

            return "unknown key usage";
        }

        public static KeyPurposeID getKeyPurposeIDFromOID(string oid) {
            if (oid == "1.3.6.1.5.5.7.3.1") {
                return KeyPurposeID.IdKPServerAuth;
            }
            if (oid == "1.3.6.1.5.5.7.3.2") {
                return KeyPurposeID.IdKPClientAuth;
            }
            if (oid == "1.3.6.1.5.5.7.3.3") {
                return KeyPurposeID.IdKPCodeSigning;
            }
            if (oid == "1.3.6.1.5.5.7.3.4") {
                return KeyPurposeID.IdKPEmailProtection;
            }
            if (oid == "1.3.6.1.5.5.7.3.5") {
                return KeyPurposeID.IdKPIpsecEndSystem;
            }
            if (oid == "1.3.6.1.5.5.7.3.6") {
                return KeyPurposeID.IdKPIpsecTunnel;
            }
            if (oid == "1.3.6.1.5.5.7.3.7") {
                return KeyPurposeID.IdKPIpsecUser;
            }
            if (oid == "1.3.6.1.5.5.7.3.8") {
                return KeyPurposeID.IdKPTimeStamping;
            }
            if (oid == "1.3.6.1.5.5.7.3.9") {
                return KeyPurposeID.IdKPOcspSigning;
            }
            if (oid == "2.5.29.37.0") {
                return KeyPurposeID.AnyExtendedKeyUsage;
            }
            if (oid == "1.3.6.1.4.1.311.20.2.2") {
                return KeyPurposeID.IdKPSmartCardLogon;
            }

            throw new Exception("unknown key usage");
        }

        public static int getKeyUsageFromString(string ku) {
            if (ku == "EncipherOnly") {
                return KeyUsage.EncipherOnly;
            } else if (ku == "CrlSign") {
                return KeyUsage.CrlSign;
            } else if (ku == "KeyCertSign") {
                return KeyUsage.KeyCertSign;
            } else if (ku == "KeyAgreement") {
                return KeyUsage.KeyAgreement;
            } else if (ku == "DataEncipherment") {
                return KeyUsage.DataEncipherment;
            } else if (ku == "KeyEncipherment") {
                return KeyUsage.KeyEncipherment;
            } else if (ku == "NonRepudiation") {
                return KeyUsage.NonRepudiation;
            } else if (ku == "DigitalSignature") {
                return KeyUsage.DigitalSignature;
            } else if (ku == "DecipherOnly") {
                return KeyUsage.DecipherOnly;
            }

            throw new Exception("Invalid key usage " + ku);
        }

        public static KeyPurposeID getExtKeyUsageFromString(string eku) {
            if (eku == "AnyExtendedKeyUsage") {
                return KeyPurposeID.AnyExtendedKeyUsage;
            } else if (eku == "IdKPClientAuth") {
                return KeyPurposeID.IdKPClientAuth;
            } else if (eku == "IdKPCodeSigning") {
                return KeyPurposeID.IdKPCodeSigning;
            } else if (eku == "IdKPEmailProtection") {
                return KeyPurposeID.IdKPEmailProtection;
            } else if (eku == "IdKPIpsecEndSystem") {
                return KeyPurposeID.IdKPIpsecEndSystem;
            } else if (eku == "IdKPIpsecTunnel") {
                return KeyPurposeID.IdKPIpsecTunnel;
            } else if (eku == "IdKPIpsecUser") {
                return KeyPurposeID.IdKPIpsecUser;
            } else if (eku == "IdKPOcspSigning") {
                return KeyPurposeID.IdKPOcspSigning;
            } else if (eku == "IdKPServerAuth") {
                return KeyPurposeID.IdKPServerAuth;
            } else if (eku == "IdKPSmartCardLogon") {
                return KeyPurposeID.IdKPSmartCardLogon;
            } else if (eku == "IdKPTimeStamping") {
                return KeyPurposeID.IdKPTimeStamping;
            }

            throw new Exception("Invalid key usage " + eku);
        }


        public static int getCertKeySize(X509Certificate cert) {
            int ks = 2048;

            if (cert.GetPublicKey().GetType() == typeof(RsaKeyParameters)) {
                ks = ((RsaKeyParameters)cert.GetPublicKey()).Modulus.BitLength;

            } else if (cert.GetPublicKey().GetType() == typeof(DsaPublicKeyParameters)) {
                ks = ((DsaPublicKeyParameters)cert.GetPublicKey()).Parameters.P.BitLength;
            }
            return ks;
        }

        public static void saveCertAndKeyToFolder(CertKeyPair kp, string filename = "cert", string folder = ".", string pass = "", bool pfx = true) {
            TextWriter textWriter = new StreamWriter(folder + "\\" + filename + ".cer");
            PemWriter pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(kp.cert);
            pemWriter.Writer.Flush();
            textWriter.Close();
            textWriter = new StreamWriter(folder + "\\" + filename + ".key");
            pemWriter = new PemWriter(textWriter);
            pemWriter.WriteObject(kp.keyPair.Private);
            pemWriter.Writer.Flush();
            textWriter.Close();
            if (pfx) {
                Pkcs12Store store = new Pkcs12StoreBuilder().Build();
                X509CertificateEntry certEntry = new X509CertificateEntry(kp.cert);
                store.SetCertificateEntry(kp.cert.SubjectDN.ToString(), certEntry); // use DN as the Alias.
                AsymmetricKeyEntry keyEntry = new AsymmetricKeyEntry(kp.keyPair.Private);
                store.SetKeyEntry(kp.cert.SubjectDN.ToString() + "_key", keyEntry, new X509CertificateEntry[] { certEntry }); // Note that we only have 1 cert in the 'chain'
                using (var filestream = new FileStream(folder + "\\" + filename + ".pfx", FileMode.Create, FileAccess.ReadWrite)) {
                    store.Save(filestream, pass.ToCharArray(), new SecureRandom());
                }
            }

         
            
        }

        public static Pkcs10CertificationRequest ReadPemCSR(String filename) {
            StreamReader sR = new StreamReader(filename);
            PemReader pR = new PemReader(sR);

            Pkcs10CertificationRequest csr= (Pkcs10CertificationRequest)pR.ReadObject();

            sR.Close();
            return csr;
        }

        public static X509Certificate signCSR(string csr, CertKeyPair CA, DateTime ValidUntil, List<KeyValuePair<DerObjectIdentifier, Asn1Encodable> > Extensions, string SigAlgo = "SHA256withRSA") {
            StringReader sr = new StringReader(csr);
            PemReader pR = new PemReader(sr); 
            Pkcs10CertificationRequest inputCSR = (Pkcs10CertificationRequest)pR.ReadObject();

            X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();

            

            v3CertGen.Reset();

            v3CertGen.SetSerialNumber(GenerateSerialNumber());
            // In this case the issuersCert.SubjectDN and issuersCert.IssuersDN 
            // are NOT the same and the subject is the correct one to use
            v3CertGen.SetIssuerDN(CA.cert.SubjectDN);
            v3CertGen.SetNotBefore(DateTime.UtcNow);
            v3CertGen.SetNotAfter(ValidUntil);
            v3CertGen.SetSubjectDN(inputCSR.GetCertificationRequestInfo().Subject);
            v3CertGen.SetPublicKey(inputCSR.GetPublicKey());

            Asn1Set certAttributes = inputCSR.GetCertificationRequestInfo().Attributes;

            List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>> parsedExtensions = new List<KeyValuePair<DerObjectIdentifier, Asn1Encodable>>();
            foreach (DerSequence attribute in certAttributes) {
                AttributeX509 attr = AttributeX509.GetInstance(attribute);

                //process extension request
                if (attr.AttrType.Id.Equals(PkcsObjectIdentifiers.Pkcs9AtExtensionRequest.Id) || attr.AttrType.Id.Equals(PkcsObjectIdentifiers.Pkcs9AtExtendedCertificateAttributes.Id)) {
                    X509Extensions extensions = X509Extensions.GetInstance(attr.AttrValues[0]);

                    var e = extensions.ExtensionOids.GetEnumerator();
                    while (e.MoveNext()) {
                        DerObjectIdentifier oid = (DerObjectIdentifier)e.Current;
                        if (oid.Id == X509Extensions.ExtendedKeyUsage.Id || oid.Id == X509Extensions.KeyUsage.Id || oid.Id == X509Extensions.SubjectAlternativeName.Id || oid.Id == X509Extensions.SubjectInfoAccess.Id) { //ToDO: Add more supported extensions here ?
                            Org.BouncyCastle.Asn1.X509.X509Extension ext = extensions.GetExtension(oid);
                            v3CertGen.AddExtension(oid, ext.IsCritical, ext.GetParsedValue());
                            parsedExtensions.Add(new KeyValuePair<DerObjectIdentifier, Asn1Encodable>(oid, ext.GetParsedValue()));
                        }
                    }
                }
               
            }





            foreach (KeyValuePair<DerObjectIdentifier, Asn1Encodable> kvext in Extensions) {
                if (!hasExtension(X509Extensions.BasicConstraints, parsedExtensions)) {
                    v3CertGen.AddExtension(kvext.Key, false, kvext.Value);
                }
            }

            if (!hasExtension(X509Extensions.BasicConstraints, Extensions) && !hasExtension(X509Extensions.BasicConstraints, parsedExtensions)) {
                v3CertGen.AddExtension(X509Extensions.CertificateIssuer, false, CA.cert.SubjectDN);
            }
            if (!hasExtension(X509Extensions.SubjectKeyIdentifier, Extensions) && !hasExtension(X509Extensions.SubjectKeyIdentifier, parsedExtensions)) {
                v3CertGen.AddExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(inputCSR.GetPublicKey()));
            }
            if (!hasExtension(X509Extensions.AuthorityKeyIdentifier, Extensions) && !hasExtension(X509Extensions.AuthorityKeyIdentifier, parsedExtensions)) {
                v3CertGen.AddExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(CA.keyPair.Public));
            }
     

            if (!hasExtension(X509Extensions.KeyUsage, Extensions) && !hasExtension(X509Extensions.KeyUsage, parsedExtensions)) {
                v3CertGen.AddExtension(X509Extensions.KeyUsage, false, new KeyUsage(KeyUsage.DigitalSignature | KeyUsage.KeyEncipherment));
            }

            //v3CertGen.AddExtension(X509Extensions.SubjectAlternativeName, true, new SubjectAlternativeName)

            Asn1SignatureFactory sig = new Asn1SignatureFactory(SigAlgo, CA.keyPair.Private, rand);
            X509Certificate cert = v3CertGen.Generate(sig);

            return cert;

        }

    public static void saveCertChainAndKeyToFolder(List<CertKeyPair> chain, string filename = "cert", string folder = ".", string pass = "", bool pfx = true) {
            TextWriter textWriter = new StreamWriter(folder + "\\" + filename + "_chain.cer");
            PemWriter pemWriter = new PemWriter(textWriter);
            foreach(CertKeyPair kp in chain) {
                pemWriter.WriteObject(kp.cert);
                pemWriter.Writer.Flush();
            }
            
            textWriter.Close();

            if (pfx) {
                Pkcs12Store store = new Pkcs12StoreBuilder().Build();
                List<X509CertificateEntry> chainx509 = new List<X509CertificateEntry>();
                foreach (CertKeyPair kp in chain) {
                    chainx509.Add(new X509CertificateEntry(kp.cert));
                    
                }
                store.SetCertificateEntry(chain.Last().cert.SubjectDN.ToString(), chainx509.Last()); // use DN as the Alias.
                AsymmetricKeyEntry keyEntry = new AsymmetricKeyEntry(chain.Last().keyPair.Private);
                store.SetKeyEntry(chain.Last().cert.SubjectDN.ToString() + "_key", keyEntry,  chainx509.ToArray() ); 
                using (var filestream = new FileStream(folder + "\\" + filename + "_chain.pfx", FileMode.Create, FileAccess.ReadWrite)) {
                    store.Save(filestream, pass.ToCharArray(), new SecureRandom());
                }
            }
        }

            #region DUMP
            /// <summary>
            /// Top level utility to  dump a certificate.  Calls all the other dump method in turn
            /// </summary>
            /// <param name="cert">The certificate to dump</param>
            public static void DumpCert(X509Certificate cert) {
            Console.WriteLine("".PadLeft(80, '-'));
            Console.WriteLine("Issuer: {0}", cert.IssuerDN);
            Console.WriteLine("Subject: {0}", cert.SubjectDN);

            int indent = 0;
            int padTo = 34;

            DumpAuthorityKeyIdentifier(cert, indent, padTo);
            DumpSubjectIdentifier(cert, indent, padTo);
            DumpExtendedKeyUse(cert, indent, padTo);

            DumpKeyUsage(cert, indent, padTo);

        }
        /// <summary>
        /// Dumps an AuthorityKeyIdentifier (the id of the issuer)
        /// </summary>
        /// <param name="cert">The certificate to dump</param>
        /// <param name="indent">How much to indent</param>
        /// <param name="padTo">How much to pad (to the right) before the value</param>
        public static void DumpAuthorityKeyIdentifier(X509Certificate cert, int indent, int padTo) {
            AuthorityKeyIdentifier keyId = CreateAuthorityKeyId(cert.GetPublicKey());
            if (keyId == null) return;

            Console.WriteLine("".PadLeft(indent * 4) + "Authority Key Identifier: {0}", ToHexString(keyId.GetKeyIdentifier()));
            if (keyId.AuthorityCertIssuer != null)
                Console.WriteLine("".PadLeft(indent * 4) + "Issuer: {0}", keyId.AuthorityCertIssuer.ToString());
            if (keyId.AuthorityCertSerialNumber != null)
                Console.WriteLine("".PadLeft(indent * 4) + "Serial number: {0}", keyId.AuthorityCertSerialNumber.ToString());

        }

        /// <summary>
        /// Creates an AuthorityKeyIdentifier based on a public key
        /// </summary>
        /// <param name="publicKey">The public key from which to generate the result</param>
        /// <returns>An AuthorityKeyIdentifier instance</returns>
        /// <exception cref="Exception">Will be thrown if the key is not public</exception>
        public static AuthorityKeyIdentifier CreateAuthorityKeyId(AsymmetricKeyParameter publicKey) {
            return new AuthorityKeyIdentifier(SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey));
        }

        /// <summary>
        /// Dumps the extended X509 attributes
        /// </summary>
        /// <param name="cert">The certificate to dump</param>
        /// <param name="indent">How much to indent</param>
        /// <param name="padTo">How much to pad (to the right) before the value</param>
        public static void DumpExtendedKeyUse(X509Certificate cert, int indent, int padTo) {
            IList list = cert.GetExtendedKeyUsage();
            if (list == null) return;
            Console.WriteLine("Extended Key Usage: ");
            foreach (string oid in list) {
                string name = "";
                if (oid == KeyPurposeID.IdKPClientAuth.Id)
                    name = "Client Authentication";
                else
                if (oid == KeyPurposeID.IdKPCodeSigning.Id)
                    name = "Code signing";
                else
                if (oid == KeyPurposeID.IdKPEmailProtection.Id)
                    name = "Email Protection";
                else
                if (oid == KeyPurposeID.IdKPIpsecEndSystem.Id)
                    name = "IPSec End System";
                else
                if (oid == KeyPurposeID.IdKPIpsecTunnel.Id)
                    name = "IPSec Tunnel";
                else
                if (oid == KeyPurposeID.IdKPIpsecUser.Id)
                    name = "IPSec User";
                else
                if (oid == KeyPurposeID.IdKPOcspSigning.Id)
                    name = "OCSP Signing";
                else
                if (oid == KeyPurposeID.IdKPServerAuth.Id)
                    name = "Server Authentication";
                else
                if (oid == KeyPurposeID.IdKPSmartCardLogon.Id)
                    name = "Smart Card Logon";
                else
                if (oid == KeyPurposeID.IdKPTimeStamping.Id)
                    name = "Time stamping";
                else
                    name = string.Format("Other ({0})", oid);

                Console.WriteLine("   {0}", name);
            }

        }
        /// <summary>
        /// Dumps the defined key use informatoin (for example whether the certificate can be used for signing, encyphering, etc)
        /// </summary>
        /// <param name="cert">The certificate to dump</param>
        /// <param name="indent">How much to indent</param>
        /// <param name="padTo">How much to pad (to the right) before the value</param>
        public static void DumpKeyUsage(X509Certificate cert, int indent, int padTo) {
            Console.WriteLine("Key usage:");
            indent = 1;

            bool[] keyUsage = cert.GetKeyUsage();
            if (keyUsage == null) {
                Console.WriteLine("".PadLeft(indent * 4) + "No key use defined");
                return;
            }

            int ku = 0;

            for (int i = 0; i < keyUsage.Length; i++) {
                if (!keyUsage[i]) continue;
                ku += 1 << i;
            }

            Console.WriteLine("".PadLeft(indent * 4) + ku);
        }

        /// <summary>
        /// Dumps the subject identifier
        /// </summary>
        /// <param name="cert">The certificate to dump</param>
        /// <param name="indent">How much to indent</param>
        /// <param name="padTo">How much to pad (to the right) before the value</param>
        public static void DumpSubjectIdentifier(X509Certificate cert, int indent, int padTo) {
            SubjectKeyIdentifier ski = GetSubjectKeyIdentifier(cert);
            if (ski == null) {
                Console.WriteLine("Subject identifier does not exist");
                return;
            }

            Console.WriteLine("Subject Key Identifier: {0}", ToHexString(ski.GetKeyIdentifier()));
        }


        /// <summary>
        /// Utility to convert a byte array to hex
        /// </summary>
        /// <param name="byteArray">The array to convert</param>
        /// <returns></returns>
        public static String ToHexString(byte[] byteArray) {
            byte[] hexEncodedArray = Org.BouncyCastle.Utilities.Encoders.Hex.Encode(
                byteArray, 0, byteArray.Length);
            String hexEncoded = Encoding.Default.GetString(hexEncodedArray);

            return hexEncoded;
        }

        /// <summary>
        /// Grabs the SubjectKeyIdentifier from an existing certificate
        /// </summary>
        /// <param name="cert">The certificate from which to grab the information</param>
        /// <returns>A SubjectKeyIdentifier instance</returns>
        public static SubjectKeyIdentifier GetSubjectKeyIdentifier(X509Certificate cert) {
            Asn1OctetString asn1string = cert.GetExtensionValue(X509Extensions.SubjectKeyIdentifier);
            if (asn1string == null) return null;

            return new SubjectKeyIdentifierStructure(asn1string);
        }
        /// <summary>
        /// Dumps the subject identifier
        /// </summary>
        /// <param name="cert">The certificate to dump</param>
        /// <param name="indent">How much to indent</param>
        /// <param name="padTo">How much to pad (to the right) before the value</param>
        #endregion
    }
}