﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.Text;

namespace easyPKI.core
{
    public class CertKeyPair
    {
        public X509Certificate cert;
        public AsymmetricCipherKeyPair keyPair;
        public CertKeyPair(X509Certificate cert, AsymmetricCipherKeyPair keyPair) {
            this.cert = cert;
            this.keyPair = keyPair;
        }
    }
}
