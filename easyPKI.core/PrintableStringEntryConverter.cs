﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using System;
using System.Collections.Generic;
using System.Text;

namespace easyPKI.core
{
    class PrintableStringEntryConverter :  X509NameEntryConverter {
        /**
     * return true if the passed in String can be represented without
     * loss as a UTF8String, false otherwise.
     */
        private bool canBeUTF8(string str) {
            for (int i = str.Length - 1; i >= 0; i--) {
                if ((int)str[i] > 0x00ff) {
                    return false;
                }
            }

            return true;
        }

        /**
         * Apply default coversion for the given value depending on the oid
         * and the character range of the value.
         * 
         * @param oid the object identifier for the DN entry
         * @param value the value associated with it
         * @return the ASN.1 equivalent for the string value.
         */
         
        public override Asn1Object GetConvertedValue(DerObjectIdentifier oid, string value) {
            if (value.Length != 0 && value[0] == '#') {
                try {
                    return ConvertHexEncoded(value, 1);
                } catch (Exception e) {
                    throw;
                    //throw new RuntimeException("can't recode value for oid " + oid.Id);
                }
            } else if (oid.Equals(X509Name.EmailAddress) || oid.Equals(X509Name.DC)) {
                return new DerIA5String(value);
            } else if (CanBePrintable(value)) {
                return new DerPrintableString(value);
            } else if (canBeUTF8(value)) {
                return new DerUtf8String(value);
            }

            return new DerBmpString(value);
        }

       
    }
}
